package usersGeneration;

public class Ratings {
	
	double[][] ratings;
	
	public Ratings(int numUsers, int numMovies){
		ratings = new double[numUsers][numMovies];
	}

	public double[][] getRatings() {
		return ratings;
	}

	public void setRatings(double[][] ratings) {
		this.ratings = ratings;
	}

}
