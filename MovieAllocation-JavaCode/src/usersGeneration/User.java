package usersGeneration;

public class User {
	
	private double[] parameters;
	//private UserUtilityComponent[] userUtilityComponents;
	private String ID;
	private double[] nonStrict;

	public User(){
		
	}

	
	public double[] getParameters() {
		return parameters;
	}

	public void setParameters(double[] parameters) {
		this.parameters = parameters;
	}

	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}


}
