package simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import inferenceFromUserActions.interactions.Selection;
import inferenceFromUserActions.interactions.TaxedSelection;
import inferenceFromUserActions.primitiveData.Assignment;
import inferenceFromUserActions.primitiveData.DiscreteAssignment;
import mipsFormulation.ResultsOfARun;
import mipsFormulation.Solution;
import usersGeneration.Ratings;

public class SimulateBehaviour {
	
	private ResultsOfARun ror;
	private int numParticipants;
	private int numOwners;
	private int numRecommendations;
//	private double[][] gtMatrix;
	private int[] positionOwners;
	private ArrayList<DiscreteAssignment> allAssignments;
	private double[][] allTaxes;
	private double[][] gt;
	private int[][] listRecommendations;
	private int[] selection;

	public SimulateBehaviour(ResultsOfARun rorPassed, 
			int numParticipantsPassed, int numOwnersPassed,
			int numRecommendationsPassed, Ratings gtRatingsClassPassed,
			int[] positionOwnersPassed, ArrayList<DiscreteAssignment> allAssignments){
		
		this.ror = rorPassed;
		this.numParticipants = numParticipantsPassed;
		this.numOwners = numOwnersPassed;
		this.numRecommendations = numRecommendationsPassed;
		this.positionOwners = positionOwnersPassed;
		this.allAssignments = allAssignments;
//		gtMatrix = gtRatingsClassPassed.getRatings();
		this.allTaxes = new double[numParticipants][numRecommendations];
		this.gt = new double[numParticipants][numRecommendations];
		
	}

	// TODO: Case with no solutions	
	// TODO: what to do when the same solution is proposed multiple times? - given the solutions selection, this should never happen
	

	public ArrayList<Selection> selectionNoise(double alphaProbabilityConstant, String noiseModel, int seed){
		
		Random random = new Random(seed);
		
		double alphaProbability = alphaProbabilityConstant;
		listRecommendations = new int[numParticipants][numRecommendations];
//		double[][] listTaxes = new double[numParticipants][numRecommendations];
		double[][] gtRatingsTaxed = new double[numParticipants][numRecommendations];

		Solution solution = ror.getFirstSolution();
		int[][] allocation = solution.getAllocation();
		double[] taxes = solution.getTax();
		double[][] gtRatings = solution.getGtRatings();
		
		
		for(int n = 0; n < numRecommendations; n++){
		for(int i = 0; i < numParticipants; i++){
			boolean exist = false;
			for(int j = 0; j < numOwners; j++){
				System.out.println("rec " + n + " user " + i+ " movie " +j+  " alloc " + allocation[i][j]);
				if(allocation[i][j] > 0.9){
					listRecommendations[i][n] = j;
//					listTaxes[i][n] = taxes[i];
					gtRatingsTaxed[i][n] = (gtRatings[i][positionOwners[j]] - taxes[i]);
					gt[i][n] = gtRatings[i][positionOwners[j]];
					allTaxes[i][n] = taxes[i];
					// if negative (because of tax), set gtTaxed to zero
					// and the tax imposed is equal to gt
					if (gtRatingsTaxed[i][n] < 0.0){
						gtRatingsTaxed[i][n] = 0.0;
						allTaxes[i][n] = gtRatings[i][positionOwners[j]];
					}
					exist = true;
					
					break;
					
				}
			} //for j
			if(exist == false){ // this case should never happen
				listRecommendations[i][n] = -1;
				System.out.println("rec " + n + " user " + i);
			}
			
			System.out.println("listRecommendations[i][n] " + listRecommendations[i][n] + " user " + i);
			
		}// for i
		// note that gtRatings are always the same!
		solution = solution.getNext();
		if(n < numRecommendations - 1){
			allocation = solution.getAllocation();
			taxes = solution.getTax();
		}
		}// for n
		
		selection = new int[numParticipants];
		ArrayList<Selection> userSelection = new ArrayList<>();
		
		for(int i = 0; i < numParticipants; i++){	
			double sumUtilityRecommendations = 0.0;
			// if first solution is different from -1 (THIS SHOULD ALWAYS BE THE CASE)
			if(listRecommendations[i][0] > -1){
			    // check how many solution are different from -1 (THERE SHOULD BE NO SOLUTION WITH -1)
				double count = 1;
				int highest = 0;
				for(int n = 1; n < numRecommendations; n++){
					if(listRecommendations[i][n] > -1){
						count++;
					} else {
						System.out.println("NON ASSIGNED RECOMMENDATION");
					}
					sumUtilityRecommendations = sumUtilityRecommendations + gtRatingsTaxed[i][n];
					// identify the recommendation with the highest groudtruth - taxation
					if(listRecommendations[i][highest] < listRecommendations[i][n]){
						highest = n;
					}
				}
				
			    // select accordingly to the probability
				
				double r = random.nextInt(100);
				
				if (noiseModel.equals("CONSTANT")){

					double beta = (100-alphaProbability)/count;
					
					for(int n = 0; n < count; n++){
						if (highest <= n && r < alphaProbability + (beta * n)){
							selection [i] = n;
							break;
						} else if (highest > n && r < beta * (n + 1)){
							selection [i] = n;
							break;
						}
					}
							
				}else if(noiseModel.equals("LOGIT")){
					double probSoFar = 0.0;
					for(int n = 0; n < count; n++){
						double increment = gtRatingsTaxed[i][n] / sumUtilityRecommendations;
						probSoFar = probSoFar + increment;
						if (r < probSoFar){
							selection[i] = n;
							break;
						}
					}
							
				} else {
					System.out.println("ERROR: NON-SUPPORTED NOISE MODEL");
				}	// end if else of noise model
				
			} else {
			// if first solution is -1 and other solution different from -1 exist
			// randomly select? select according to the utility with constant model?
				
			// THIS CASE SHOULD NEVER HAPPEN WITH THE MOVIE ALLOCATION
				System.out.println("ERROR: A CASE THAT WAS NOT SUPPOSED TO HAPPEN, EVER, JUST HAPPENED");
			}
			
			Selection sel = computeUserSelection(selection[i], listRecommendations[i], i);
			userSelection.add(sel);
			
		}//for i
		
		return userSelection;
	}
	
	
	
	public Selection computeUserSelection(int selected, int[] recommended, int user){
		
		List<Assignment> assignments = new ArrayList<>();
		DiscreteAssignment assignment =  null;
		List<Double> taxation = new ArrayList<>();
		
		for(int rec = 0; rec < recommended.length; rec++){
			assignment = allAssignments.get(recommended[rec]);
			assignments.add(assignment);
			taxation.add(allTaxes[user][rec]);
		}
		
		Selection sel = new TaxedSelection(assignments, taxation, selected);
		
		return sel;
	}
	
	public double[][] getAllTaxes() {
		return allTaxes;
	}

	public double[][] getGt() {
		return gt;
	}

	public int[][] getListRecommendations() {
		return listRecommendations;
	}
	
	public int[] getSelection() {
		return selection;
	}
	
}
