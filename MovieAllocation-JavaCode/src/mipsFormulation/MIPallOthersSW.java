package mipsFormulation;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.DoubleParam;
import usersGeneration.User;

public class MIPallOthersSW {

	private double[][] v;

	// double systemUtility;
	// double fairness;
	// double allocatedPassengers;
	// double[] passengersUtility;
	int[][][] allocation;

	boolean solved;

	private double timeLimitInSecs;
	private int numParticipants;
	private int numOwners;
	private int minimumUserWithSameRecommendation;
	private double bigM = 99999999.0;
	private ResultsOfARun ror;
	private int numRecommendationsToGo;
	private String noiseModel;
	private double epsilon;
	private double[][] taxesValue;
	private double[] firstPassengersUtility;
	private IloLinearNumExpr allocationComparison;
	private String model;

	IloCplex cplex;

	public MIPallOthersSW(double[][] estimatedRatings, int numUsers, int numMovies,
			int minimumUserWithSameRecommendation, ResultsOfARun ror, int numRecommendationsToGo, String noiseModel,
			double epsilon, String modelPassed, IloCplex cplexOther, double timeLimitInSecs) {

		this.timeLimitInSecs = timeLimitInSecs;
		this.numParticipants = numUsers;
		this.numOwners = numMovies;
		this.minimumUserWithSameRecommendation = minimumUserWithSameRecommendation;
		this.ror = ror;
		this.numRecommendationsToGo = numRecommendationsToGo;
		this.noiseModel = noiseModel;
		this.epsilon = epsilon;
		this.model = modelPassed;

		allocation = new int[numParticipants][numOwners][numRecommendationsToGo];
		taxesValue = new double[numParticipants][numRecommendationsToGo];

		v = estimatedRatings;

		this.cplex = cplexOther;

		program();

	}

	public void program() {
		try {

			// instance of the engine
			cplex.clearModel();
			cplex.setParam(DoubleParam.TiLim, timeLimitInSecs);
			// IloCplex cplex = new IloCplex();

			// variables --- allocation: x_i,j
			// 0 no i in j, 1 i in j
			IloIntVar[][][] x = new IloIntVar[numParticipants][numOwners][numRecommendationsToGo];
			for (int i = 0; i < numParticipants; i++) {
				for (int j = 0; j < numOwners; j++) {
					x[i][j] = cplex.boolVarArray(numRecommendationsToGo);
					for (int h = 0; h < numRecommendationsToGo; h++) {
						x[i][j][h].setName("x_" + i + "_" + j + "_" + h);
					}
				}
			}

			// taxes
			IloNumVar[][][] taxes = new IloNumVar[numParticipants][numOwners][numRecommendationsToGo];
			for (int i = 0; i < numParticipants; i++) {
				for (int j = 0; j < numOwners; j++) {
					taxes[i][j] = cplex.numVarArray(numRecommendationsToGo, 0.0, 10.0); // bigM
					for (int h = 0; h < numRecommendationsToGo; h++) {
						taxes[i][j][h].setName("taxes_" + i + "_" + j + "_" + h);
					}
				}
			}

			// k variable
			IloIntVar[][] k = new IloIntVar[numOwners][];
			for (int j = 0; j < numOwners; j++) {
				k[j] = cplex.boolVarArray(numRecommendationsToGo);
				for (int h = 0; h < numRecommendationsToGo; h++) {
					k[j][h].setName("k_" + j + "_" + h);
				}
			}

			// variable dummy
			IloNumVar dummy = cplex.numVar(1, 1);
			dummy.setName("dummy");

			// OBJECTIVE
			IloLinearNumExpr obj = cplex.linearNumExpr();
			for (int h = 0; h < numRecommendationsToGo; h++) {
				for (int i = 0; i < numParticipants; i++) {
					for (int j = 0; j < numOwners; j++) {
						obj.addTerm(v[i][j], x[i][j][h]);
					}
				}
			}

			int[][] allocationFirst = ror.getFirstSolution().getAllocation();
			
			//TODO delete output
//			for (int i = 0; i < numParticipants; i++) {
//				for (int j = 0; j < numOwners; j++) {
//					System.out.println("i, j : " + i + ", " + j + " : " + v[i][j]);
//				}
//			}
			
			
			if (model.equals("OUR")) {
				double temp;
				double[][] estimatedRatingsFirst = ror.getFirstSolution().getEstimatedRatings();
				//TODO rounding up estimetedRatings
				for ( int i = 0; i < estimatedRatingsFirst.length; i++ ) {
					for ( int j = 0; j < estimatedRatingsFirst[0].length; j++ ) {
						temp = Math.round( estimatedRatingsFirst[i][j] * 1000 );
						temp = temp / 1000;
						estimatedRatingsFirst[i][j] = temp;
					}
				}
				
				
				// int[][] allocationFirst = ror.getFirstSolution().getAllocation(); 

				firstPassengersUtility = new double[numParticipants];

				for (int i = 0; i < numParticipants; i++) {
					for (int j = 0; j < numOwners; j++) {
						firstPassengersUtility[i] = firstPassengersUtility[i]
								+ (estimatedRatingsFirst[i][j] * allocationFirst[i][j]); //TODO maybe allocation first is wrong?
					}
					// TODO delete output
//					System.out.println("the utility for passenger " + i + " is " + firstPassengersUtility[i]);
				}

				if (noiseModel.equals("CONSTANT")) {

					//TODO delete block comment
					for (int h = 0; h < numRecommendationsToGo; h++) {
						for (int i = 0; i < numParticipants; i++) {
							//obj.addTerm(-bigM * (firstPassengersUtility[i] - epsilon), dummy); //TODO commented out this. Why is this here?!
							for (int j = 0; j < numOwners; j++) {
								//obj.addTerm(bigM * v[i][j], x[i][j][h]); //TODO commented out this. Why is this here?!
								obj.addTerm(-1, (IloNumVar) taxes[i][j][h]); // was times -bigM
							}
						}
					}

				} else if (noiseModel.equals("LOGIT")) {

					for (int i = 0; i < numParticipants; i++) {
						obj.addTerm(-bigM * firstPassengersUtility[i] * (1 - epsilon), dummy);
						for (int h = 0; h < numRecommendationsToGo; h++) {
							for (int j = 0; j < numOwners; j++) {
								obj.addTerm(bigM * epsilon * v[i][j], x[i][j][h]);
								obj.addTerm(-bigM * epsilon, taxes[i][j][h]);
							}
						}
					}

				} else {
					System.out.print("NOISE MODEL NOT RECOGNIZED");
				}
			} // end OUR model

			cplex.addMaximize(obj);

			// ROOMS constraints
			//TODO remove comment block
			IloLinearNumExpr sumForKj;
			for (int h = 0; h < numRecommendationsToGo; h++) {
				for (int j = 0; j < numOwners; j++) {
					sumForKj = cplex.linearNumExpr();
					for (int i = 0; i < numParticipants; i++) {
						sumForKj.addTerm(1, x[i][j][h]);
					}

					// sum_i x_ij >= m kj
					// cplex.addGe(cplex.sum(sumForKj, cplex.prod(-minimumUserWithSameRecommendation, k[j][h])), 0); // This and the one after were the same constraint effectively.

					// m - sum_i x_ij <= (1 - kj) M
					cplex.addLe( cplex.negative(sumForKj), // cplex.sum(minimumUserWithSameRecommendation, cplex.negative(sumForKj))
							cplex.prod(minimumUserWithSameRecommendation, cplex.negative(k[j][h]) ) ); // cplex.prod(bigM, cplex.sum(1, cplex.negative(k[j][h]))))

					// (m - 1) - sum_i x_ij >= kj (-M)
					// cplex.addGe(cplex.sum(minimumUserWithSameRecommendation-1,
					// cplex.negative(sumForKj)), cplex.prod(-bigM, k[j][h]));
					// // TODO Seems redundant!

					// kj M >= sum_i x_i,j
					cplex.addGe(cplex.prod(bigM, k[j][h]), sumForKj);

				}
			}

			// CONSTRAINTS ON VARIABLE x
			//TODO remove comment block
			for (int h = 0; h < numRecommendationsToGo; h++) {
				for (int i = 0; i < numParticipants; i++) {
					IloLinearNumExpr sumOverJ = cplex.linearNumExpr();
					for (int j = 0; j < numOwners; j++) {
						sumOverJ.addTerm(1, x[i][j][h]); // Each user, for each recommended global solution, should have 1 recommendation.
					}

					cplex.addLe(sumOverJ, 1);
					cplex.addGe(sumOverJ, 0.9);
				}
			}

			// CONSTRAINTS DIFFERENT SOLUTIONS

			Solution current = ror.getFirstSolution();

			int[][] previousAllocation = current.getAllocation();
			IloNumVar absVar;
			
			for (int h = 0; h < numRecommendationsToGo; h++) {

				// comparison with the first solution
				// previousAllocation = current.getAllocation();

				//TODO remove comment block
				for (int i = 0; i < numParticipants; i++) {

					IloLinearNumExpr allocationComparisonWithFirst = cplex.linearNumExpr();

					for (int j = 0; j < numOwners; j++) {
						absVar = (IloNumVar) cplex.abs(cplex.sum(-previousAllocation[i][j], x[i][j][h]));
						absVar.setName("abs_sum_minusPreviousAlloc_" + i + "_" + j + "_and_X_" + i + "_" + j + "_" + h);
						allocationComparisonWithFirst.addTerm(1, absVar);
					}

					cplex.addGe(allocationComparisonWithFirst, 1.0); // Well
																		// it's
																		// either
																		// 0 or
																		// 2, so
																		// we
																		// are
																		// forcing
																		// the
																		// latter.
				}

				// comparison with the other solutions here computed
				//TODO remove comment block
				for (int hh = h + 1; hh < numRecommendationsToGo; hh++) {

					for (int i = 0; i < numParticipants; i++) {

						// IloLinearNumExpr allocationComparison =
						// cplex.linearNumExpr();
						allocationComparison = cplex.linearNumExpr();

						for (int j = 0; j < numOwners; j++) {
							absVar = (IloNumVar) cplex.abs(cplex.sum(cplex.prod(-1, x[i][j][hh]), x[i][j][h]));
							absVar.setName("abs_sum_minusX_" + i + "_" + j + "_" + hh + "_and_X_" + i + "_" + j + "_" + h);
							allocationComparison.addTerm(1, absVar); // TODO
																												// why
																												// is
																												// allocationComparison
																												// treated
																												// differently
																												// from
																												// allocationComparisonWithFirst?
						}

						cplex.addGe(allocationComparison, 1.0);

					}

				} // end of hh

			} // end for h

			// CONSTRAINT: MINIMUM TAXATION

			if (model.equals("OUR")) {

				IloLinearNumExpr utilI;

				if (noiseModel.equals("CONSTANT")) {
//TODO remove comment block
					for (int h = 0; h < numRecommendationsToGo; h++) {

						for (int i = 0; i < numParticipants; i++) {
							// if(firstPassengersUtility[i] > 0.0){

							//utilI = cplex.linearNumExpr(); // TODO Separating the taxation constraints

							for (int j = 0; j < numOwners; j++) {
								utilI = cplex.linearNumExpr();
								utilI.addTerm(v[i][j], x[i][j][h]);
								utilI.addTerm(-1, taxes[i][j][h]);
								cplex.addLe(utilI, firstPassengersUtility[i] - epsilon);
							}

							//cplex.addLe(utilI, firstPassengersUtility[i] - epsilon);
							// }

						}

					} // end for h

				} else if (noiseModel.equals("LOGIT")) {

					IloLinearNumExpr utilAllOthersForI = cplex.linearNumExpr();
					//;

					for (int i = 0; i < numParticipants; i++) {

						utilAllOthersForI = cplex.linearNumExpr();

						for (int h = 0; h < numRecommendationsToGo; h++) {

							for (int j = 0; j < numOwners; j++) {

								utilAllOthersForI.addTerm(v[i][j], x[i][j][h]);
								utilAllOthersForI.addTerm(-1, taxes[i][j][h]);

							}

						} // end for h

						cplex.addGe((1 - epsilon) * firstPassengersUtility[i], cplex.prod(epsilon, utilAllOthersForI));

					} // end for i

				} // end else

				// CONSTRAINT on TAXES: if x = 0, then taxes = 0, otherwise no real constraint
				//TODO remove comment block
				for (int h = 0; h < numRecommendationsToGo; h++) {
					for (int i = 0; i < numParticipants; i++) {
						for (int j = 0; j < numOwners; j++) {
							cplex.addLe(taxes[i][j][h], cplex.prod(bigM, x[i][j][h])); // TODO .le -> .addLe???? I don't think this constraint has ever been active up till now.
						}
					}
				}

				// set initial conditions
				// allocationFirst
				//TODO remove comment block
				double tax;
				int[] usedIndices = new int[numOwners];
				
				for ( int owner = 0; owner < numOwners; owner++ ) {
					for ( int participant = 0; participant < numParticipants; participant++ ) {
						if ( allocationFirst[participant][owner] > 0 ) {
							usedIndices[owner] = 1;
							break;
						}
					}
				}
				
				double[][][] taxes_demo = new double[numParticipants][numOwners][numRecommendationsToGo];
				int[][][] x_demo = new int[numParticipants][numOwners][numRecommendationsToGo];
				int[][] k_demo = new int[numOwners][numRecommendationsToGo];
				double dummy_demo = 1;
				
				int currentIndexUsedUp = 1;
				int currentIndex = 0;
				int newIndexNotFound;
				int timesIndexUsed = 0;
				for ( int h = 0; h < numRecommendationsToGo; h++ ) {
					currentIndexUsedUp = 1;
					//currentIndex = 0;
					
					for ( int participant = 0; participant < numParticipants; participant++ ) {
						if ( currentIndexUsedUp == 1 ) {
							newIndexNotFound = 1;
							while ( newIndexNotFound == 1) {
								if ( usedIndices[currentIndex] == 1 ) {
									currentIndex++;
								} else {
									newIndexNotFound = 0;
								}
							}
							currentIndexUsedUp = 0;
							timesIndexUsed = 0;
							k_demo[currentIndex][h] = 1;
							usedIndices[currentIndex] = 1;
							//TODO delete output:
							System.out.println("k_demo_" + currentIndex + "_" + h + " = 1");
						}
						
						tax = v[participant][currentIndex] - firstPassengersUtility[participant] + epsilon;
						if ( tax < 0 ) {
							System.out.println("Tax was lower than 0!");
							tax = 0;
						}
						taxes_demo[participant][currentIndex][h] = tax;
						//TODO delete output:
						System.out.println("taxes_demo" + participant + "_" + currentIndex + "_" + h + " = " + tax);
						
						x_demo[participant][currentIndex][h] = 1;
						timesIndexUsed++;
						//TODO delete output:
						System.out.println("x_demo" + participant + "_" + currentIndex + "_" + h + " = 1");
						
						if ( timesIndexUsed >= minimumUserWithSameRecommendation ) {
							currentIndexUsedUp = 1;
						}
					}
				}
				// x_demo, taxes_demo, k_demo, dummy_demo
				IloNumVar[] variable = new IloNumVar[  numOwners * numRecommendationsToGo * (2*numParticipants + 1) ]; //  + 1 for dummy
				double[] value = new double[ numOwners * numRecommendationsToGo * (2*numParticipants + 1) ]; //numOwners * numRecommendationsToGo * (1*numParticipants + 0) + 1 for dummy
				
				int variableLinearIndex = 0;
				
				for ( int i = 0; i < numParticipants; i++ ) {
					for ( int j = 0; j < numOwners; j++ ) {
						for ( int h = 0; h < numRecommendationsToGo; h++ ) {
							
							variable[variableLinearIndex] = x[i][j][h];
							value[variableLinearIndex] = x_demo[i][j][h];
							
							variableLinearIndex++;
							
						}
					}
				}
				
				for ( int i = 0; i < numParticipants; i++ ) {
					for ( int j = 0; j < numOwners; j++ ) {
						for ( int h = 0; h < numRecommendationsToGo; h++ ) {
							
							variable[variableLinearIndex] = taxes[i][j][h];
							value[variableLinearIndex] = taxes_demo[i][j][h];
														
							variableLinearIndex++;
							
						}
					}
				}
				
				for (int j = 0; j < numOwners; j++) {
					for (int h = 0; h < numRecommendationsToGo; h++) {

						variable[variableLinearIndex] = k[j][h];
						value[variableLinearIndex] = k_demo[j][h];
						
						variableLinearIndex++;

					}
				}
				
				
				//variable[variableLinearIndex] = dummy;
				//value[variableLinearIndex] = dummy_demo;
				
				cplex.addMIPStart( variable, value );
				

			} // end OUR model

			// solve
			// cplex.setParam(IloCplex.DoubleParam.TiLim, 7200);
			String filename = "MIPallOthersSW" + "potato" + ".lp";
			cplex.exportModel(filename);
			solved = cplex.solve();
			if (solved) {
				System.out.println("MIP Others solved");
			} else {
				System.out.println("MIP Others NOT solved");
			}

			// GET THE INFORMATION WE NEED

			// // passengers utilities
			// for (int i = 0; i < numParticipants; i++) {
			// IloLinearNumExpr passUtility = cplex.linearNumExpr();
			// for (int n = 0; n < numOwners; n++) {
			// passUtility.addTerm(v[i][n], x[i][n]);
			// }
			// passengersUtility[i] = cplex.getValue(passUtility);
			//
			// System.out.println("student " + i + " " + passengersUtility[i] +
			// " ");
			// }
			//
			// // utility of the system
			// systemUtility = 0;
			//
			// for (int i = 0; i < numParticipants; i++) {
			// systemUtility = systemUtility + passengersUtility[i];
			// }
			//
			// System.out.println("systemUtility " + systemUtility);
			//
			// // fairness of the solution
			// fairness = 0;
			// for (int i = 0; i < numParticipants; i++) {
			// for (int k = i + 1; k < numParticipants; k++) {
			// fairness = fairness + Math.abs(passengersUtility[i] -
			// passengersUtility[k]);
			// }
			// }

			// allocation
			for (int h = 0; h < numRecommendationsToGo; h++) {
				// System.out.println("rec " + h);
				for (int i = 0; i < numParticipants; i++) {
					for (int j = 0; j < numOwners; j++) {
						allocation[i][j][h] = (int) Math.round(cplex.getValue(x[i][j][h]));
						// System.out.print(allocation[i][j][h] + " ");
						// if(allocation[i][j][h] == 1){
						// System.out.println("rec " + h + " user " + i + "
						// movie " + j );
						// }
						double temp = cplex.getValue(x[i][j][h]);
						if (temp > 0) {
							taxesValue[i][h] = cplex.getValue(taxes[i][j][h]); //TODO seems taxes might be stored wrongly if there is some inaccuracy near 0
						}
					}
					// System.out.println();
				}
				// System.out.println();
				// System.out.println();
			}

			// double[] temp = cplex.getValues(ut);
			// for( int i = 0; i < numParticipants; i++){
			// System.out.println("ut " + i + " " + temp[i]);
			// }

			// System.out.println("constraint " +
			// cplex.getValue(allocationComparison));

			// cplex.end();

		} catch (IloException e) {
			e.printStackTrace();
		}
	}

	// public double getSystemUtility() {
	// return systemUtility;
	// }
	//
	// public double getFairness() {
	// return fairness;
	// }
	//
	// public double[] getPassengersUtility() {
	// return passengersUtility;
	// }

	public int[][][] getAllocation() {
		return allocation;
	}

	public double[][] getTaxes() {
		return taxesValue;
	}

	public boolean getSolved() {
		return solved;
	}

}
