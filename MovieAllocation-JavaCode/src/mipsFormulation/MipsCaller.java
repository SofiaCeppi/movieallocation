package mipsFormulation;

import java.util.Date;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import usersGeneration.Ratings;

public class MipsCaller {
	
	private double timeLimitInSecs;
	
	private int numUsers;
	private int numMovies;
	private String noiseModel;
	private int numRecommendations;
	private double epsilon;
	private String experiment;
	private int minimumUserWithSameRecommendation;
	private String model;
	private long difference;
	
	public MipsCaller(){
		
	}
	
	public ResultsOfARun callMips(Ratings estimatedRatingsClass, Ratings gtRatingsClass,
			int numUsersPassed, int numMoviesPassed, String noiseModelPassed, int numRecommendationsPassed,
			double epsilonPassed, String experimentPassed, int minimumUserWithSameRecommendationPassed, String modelPassed, IloCplex cplex, IloCplex cplexOther, double timeLimitInSecs){
		
		long lStartTime = new Date().getTime();
		
		this.numRecommendations = numRecommendationsPassed;
		double initialLevelSystemUtility = 0.5;
		this.numMovies = numMoviesPassed;
		this.numUsers = numUsersPassed;
		this.minimumUserWithSameRecommendation = minimumUserWithSameRecommendationPassed;
		this.noiseModel = noiseModelPassed;
		this.epsilon = epsilonPassed;
		this.experiment = experimentPassed;
		this.model = modelPassed;
				
		this.timeLimitInSecs = timeLimitInSecs;
				
		
		double[][] estimatedRatings = estimatedRatingsClass.getRatings();
		
		// TODO delete output:
//		for ( int i = 0; i < estimatedRatings.length; i++ ) {
//			for ( int j = 0; j < estimatedRatings[i].length; j++ ) {
//				System.out.println( "This is Sparta! " + estimatedRatings[i][j] );
//			}
//		}
		
		double[][] gtRatings = gtRatingsClass.getRatings();
		
		// MIP system
		ResultsOfARun ror = new ResultsOfARun();
		
		MIPsystem mipSystem = new MIPsystem(estimatedRatings, numUsers, numMovies, minimumUserWithSameRecommendation, cplex, timeLimitInSecs);
		
		boolean solved = mipSystem.getSolved();
		if (solved) {
			double systemUtility = mipSystem.getSystemUtility();
			double maxSysUtil = systemUtility;
			int[][] allocation = mipSystem.getAllocation();
			double[] noTax = new double[numUsers];
			for(int i = 0; i < numUsers; i++){
				noTax[i] = 0.0;
			}

			Solution bestForSystem = new Solution(estimatedRatings, gtRatings, 
					allocation, noTax, 1.0, noiseModel, epsilon, 
					minimumUserWithSameRecommendation, numRecommendations);
			
			ror.setBestForSystem(bestForSystem);
			
			double levelSystemUtility = initialLevelSystemUtility;
			
			// MIP First
			
//			MIPfirst mipFirst = new MIPfirst(maxSysUtil, levelSystemUtility, estimatedRatings, numUsers, numMovies, minimumUserWithSameRecommendation);

			if(experiment.equals("SocialWelfare")){
				solved = mipSystem.getSolved();
			} else if (experiment.equals("Fairness")){
//				solved = mipFirst.getSolved();
			}
			if (solved) {
				Solution first = null;
				if(experiment.equals("Fairness")){
//					allocation = mipFirst.getAllocation();
//				    first = new Solution(estimatedRatings, gtRatings, 
//						allocation, noTax, levelSystemUtility, noiseModel, epsilon, 
//						minimumUserWithSameRecommendation, numRecommendations);
				} else {
					first = new Solution(estimatedRatings, gtRatings, 
							allocation, noTax, 1.0, noiseModel, epsilon, 
							minimumUserWithSameRecommendation, numRecommendations);
				}
				
				ror.setFirstSolution(first);
				
				
				
				boolean found = false;
				while(found == false){
				
					MIPallOthersSW mipAllOthersSW = new MIPallOthersSW (estimatedRatings, numUsers,
							numMovies, minimumUserWithSameRecommendation, ror, numRecommendations - 1, 
							noiseModel, epsilon, model, cplexOther, timeLimitInSecs);
				
				solved = mipAllOthersSW.getSolved();
				
				if(solved){
					
					found = true;
					
					int[][][] allocationOthers = mipAllOthersSW.getAllocation();
					double[][] taxesOthers = mipAllOthersSW.getTaxes();
					
					int[][] oneAllocation;
					double[] oneTaxes;
					
					for(int o = 0; o < numRecommendations-1; o++) {//number of other recommendation 

						oneAllocation = new int[numUsers][numMovies];
						oneTaxes = new double[numUsers];
						
						for(int i = 0; i < numUsers; i++){
							oneTaxes[i] = taxesOthers[i][o];
							for(int j = 0; j < numMovies; j++){
								oneAllocation[i][j] = allocationOthers[i][j][o];
							}
						}
						
						Solution other = new Solution(estimatedRatings, gtRatings, 
								oneAllocation, oneTaxes, 1.0, noiseModel, epsilon, 
								minimumUserWithSameRecommendation, numRecommendations);
						
						first.setNext(other);
						first = first.getNext();
					}// end for number of other recommendation
					
				} else { //not solve
			 		// TODO: reduce the number of total recommendations
					timeLimitInSecs = timeLimitInSecs * 2;
					String filename = "MIPallOthersSW" + "fromCaller" + ".lp";
					try {
						cplexOther.exportModel(filename);
					} catch (IloException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
					
				} 
				//end while
				

				
				
//				if(noiseModel.equals("CONSTANT")){
//				// repeated MIP others
////				for(int i = 1; i < numRecommendations; i++){
////					
////					if(experiment.equals("Fairness")){
////						
////						// TODO: update how different solutions are computed in MIPothers
////						// and then just remove the comment to the section below
////		
//////					MIPothers mipOthers = new MIPothers(maxSysUtil, levelSystemUtility, estimatedRatings, 
//////							numUsers, numMovies, minimumUserWithSameRecommendation, ror, i, noiseModel, epsilon);
//////					
//////					solved = mipOthers.getSolved();
//////					if(solved){
//////						allocation = mipOthers.getAllocation();
//////						double[] taxes = mipOthers.getTaxes();
//////						
//////						Solution other = new Solution(estimatedRatings, gtRatings, 
//////								allocation, taxes, levelSystemUtility, noiseModel, epsilon, 
//////								minimumUserWithSameRecommendation, numRecommendations);
//////						
//////						first.setNext(other);
//////						first = first.getNext();
//////					} else {// End IF Mip Others solved - ELSE I reduce the levelSystemUtility
//////						
//////						if(levelSystemUtility > 0.0){
//////							levelSystemUtility = levelSystemUtility - 0.1;
//////							if(levelSystemUtility < 0.0){
//////								levelSystemUtility = 0.0;
//////							}
//////							i--;
//////						}
//////						
//////					}
////					}// end if experiment is "Fairness"
////					else if (experiment.equals("SocialWelfare")){
////						
////						// TODO: SocialWelfare CONSTANT
////						MIPothersSW mipOthersSW = new MIPothersSW(maxSysUtil, levelSystemUtility, estimatedRatings, 
////								numUsers, numMovies, minimumUserWithSameRecommendation, ror, i, noiseModel, epsilon);
////						
////						solved = mipOthersSW.getSolved();
////						if(solved){
////							allocation = mipOthersSW.getAllocation();
////							double[] taxes = mipOthersSW.getTaxes();
////							
////							Solution other = new Solution(estimatedRatings, gtRatings, 
////									allocation, taxes, 1.0, noiseModel, epsilon, 
////									minimumUserWithSameRecommendation, numRecommendations);
////							
////							first.setNext(other);
////							first = first.getNext();
////						} else {
////							
////							// TODO: what if there are not enough recommendations?
////							// Should I reset the number of recommendation in the main?
////							
////						}
////						
////					}// end if experiment is "SocialWelfare"
////				} // End for num Recommendations
//					
//					boolean found = false;
//					while(found == false){
//					
//						MIPallOthersSW mipAllOthersSW = new MIPallOthersSW (estimatedRatings, numUsers, numMovies, minimumUserWithSameRecommendation, ror, numRecommendations - 1, noiseModel, epsilon);
//					
//					solved = mipAllOthersSW.getSolved();
//					
//					if(solved){
//						
//						found = true;
//						
//						int[][][] allocationOthers = mipAllOthersSW.getAllocation();
//						double[][] taxesOthers = mipAllOthersSW.getTaxes();
//						
//						int[][] oneAllocation;
//						double[] oneTaxes;
//						
//						for(int o = 1; o < numRecommendations; o++) {//number of other recommendation 
//
//							oneAllocation = new int[numUsers][numMovies];
//							oneTaxes = new double[numUsers];
//							
//							for(int i = 0; i < numUsers; i++){
//								oneTaxes[i] = taxesOthers[i][o];
//								for(int j = 0; j < numMovies; j++){
//									oneAllocation[i][j] = allocationOthers[i][j][o];
//								}
//							}
//							
//							Solution other = new Solution(estimatedRatings, gtRatings, 
//									oneAllocation, oneTaxes, 1.0, noiseModel, epsilon, 
//									minimumUserWithSameRecommendation, numRecommendations);
//							
//							first.setNext(other);
//    						first = first.getNext();
//						}// end for number of other recommendation
//						
//					} else { //not solve
//				 		
//						// TODO: reduce the number of total recommendations
//					}
//						
//					} 
//					//end while
//					
//				}// end if experiment is "SocialWelfare"
//					
//					
//			} // end CONSTANT case
//			
//			else if (noiseModel.equals("LOGIT")){
//				
//				if(experiment.equals("Fairness")){
//					
//					// TODO: fairness case LOGIT
//					System.out.println("THIS PART OF THE CODE IS MISSING BECAUSE"
//							+ "THE PLAN IS NOT TO TEST THIS (AT LEAST FOR THE MOMENT");
//					
//				}// end if experiment is "Fairness"
//				else if (experiment.equals("SocialWelfare")){
//					
//					// TODO: SocialWelfare LOGIT
//					boolean found = false;
//					while(found == false){
//					
//						MIPallOthersSW mipAllOthersSW = new MIPallOthersSW (estimatedRatings, numUsers, numMovies, minimumUserWithSameRecommendation, ror, numRecommendations - 1, noiseModel, epsilon);
//					
//					solved = mipAllOthersSW.getSolved();
//					
//					if(solved){
//						
//						found = true;
//						
//						int[][][] allocationOthers = mipAllOthersSW.getAllocation();
//						double[][] taxesOthers = mipAllOthersSW.getTaxes();
//						
//						int[][] oneAllocation;
//						double[] oneTaxes;
//						
//						for(int o = 1; o < numRecommendations; o++) {//number of other recommendation 
//
//							oneAllocation = new int[numUsers][numMovies];
//							oneTaxes = new double[numUsers];
//							
//							for(int i = 0; i < numUsers; i++){
//								oneTaxes[i] = taxesOthers[i][o];
//								for(int j = 0; j < numMovies; j++){
//									oneAllocation[i][j] = allocationOthers[i][j][o];
//								}
//							}
//							
//							Solution other = new Solution(estimatedRatings, gtRatings, 
//									oneAllocation, oneTaxes, 1.0, noiseModel, epsilon, 
//									minimumUserWithSameRecommendation, numRecommendations);
//							
//							first.setNext(other);
//    						first = first.getNext();
//						}// end for number of other recommendation
//						
//					} else { //not solve
//				 		
//						// TODO: reduce the number of total recommendations
//					}
//						
//					} 
//					//end while
//					
//				}// end if experiment is "SocialWelfare"
//				
//			}// end LOGIT case
				
				
			} // Enf IF Mip First Solved
			
		} // End IF Mip System Solved
		
		long lEndTime = new Date().getTime();

		difference = lEndTime - lStartTime;
		
		return ror;
	}

	public long getDifference(){
		return difference;
	}

}
