package mipsFormulation;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;


public class MIPfirst {

	private double[][] v;
    
//    double systemUtility;
//    double fairness;
//    double allocatedPassengers;
//    double[] passengersUtility;
    int[][] allocation;
    
    boolean solved;
    
    int numParticipants;
    int numOwners;
    int minimumPerRoom;
    
    double bigM = 99999999;
    
	private double maxSystemUtility;
	private double levelSystemUtility;


	public MIPfirst(double maxSystemUtility, double levelSystemUtility, 
			double[][] estimatedRatings, int numUsers, int numMovies, int minimumPerRoom) {

		this.maxSystemUtility = maxSystemUtility;
		this.levelSystemUtility = levelSystemUtility;
		this.numParticipants = numUsers;
		this.numOwners = numMovies;
		this.minimumPerRoom = minimumPerRoom;
		
//		passengersUtility = new double[numParticipants];
		allocation = new int[numParticipants][numOwners];
		
		v = estimatedRatings;

		program();

	}

	public void program() {
		try {

			// instance of the engine
			IloCplex cplex = new IloCplex();

			// variables --- allocation: x_i,j
			// 0 no i in j, 1 i in j
			IloIntVar[][] x = new IloIntVar[numParticipants][];
			for (int i = 0; i < numParticipants; i++) {
				x[i] = cplex.boolVarArray(numOwners);
			}

			 // k variable
			 IloNumVar[] k = cplex.boolVarArray(numOwners);
			 
			 // abs value
			 IloNumVar[][] abs = new IloNumVar[numParticipants][];
				for (int i = 0; i < numParticipants; i++) {
					abs[i] = cplex.numVarArray(numParticipants, 0, Double.MAX_VALUE);
				}
			  
			

				// OBJECTIVE
			 IloLinearNumExpr obj = cplex.linearNumExpr();

				for (int i = 0; i < numParticipants; i++) {
					for (int h = i + 1; h < numParticipants; h++) {
						obj.addTerm(1, abs[i][h]);
					}
				}

				cplex.addMinimize(obj);

				// CONSTRAINT TO COMPUTE THE ABS VALUE
				for (int i = 0; i < numParticipants; i++) {

					IloLinearNumExpr utilI = cplex.linearNumExpr();
					for (int n = 0; n < numOwners; n++) {
						utilI.addTerm(v[i][n], x[i][n]);
					}

					for (int h = 0; h < numParticipants; h++) {

						IloLinearNumExpr utilK = cplex.linearNumExpr();

						for (int n = 0; n < numOwners; n++) {
							utilK.addTerm(v[h][n], x[h][n]);
						}

						cplex.addGe(cplex.sum(abs[i][h],
								cplex.negative(cplex.abs(cplex.sum(utilI, cplex.negative(utilK))))), 0.0);
						cplex.addLe(cplex.sum(abs[i][h],
								cplex.negative(cplex.abs(cplex.sum(utilI, cplex.negative(utilK))))), 0.0);
					}
				}

				
				
				// CONSTRAINT: LEVEL OF SYSTEM UTILITY

				IloLinearNumExpr sysUtil = cplex.linearNumExpr();
				// add social welfare to objective (weight w[0])
				for (int i = 0; i < numParticipants; i++) {
					for (int n = 0; n < numOwners; n++) {
						sysUtil.addTerm(v[i][n], x[i][n]);
					}
				}

				System.out.println("maxSysUtil " + maxSystemUtility);
				System.out.println("levelSystemUtility " + levelSystemUtility);

				cplex.addGe(sysUtil, maxSystemUtility * levelSystemUtility);
			
			
				// ROOMS constraints
				IloLinearNumExpr sumForKj;
				for(int j = 0; j < numOwners; j++){
					sumForKj = cplex.linearNumExpr();
					for(int i = 0; i < numParticipants; i++){
						sumForKj.addTerm(1, x[i][j]);
					}
					
					// sum_i x_ij >= m kj
					cplex.addGe(cplex.sum(sumForKj, cplex.prod(-minimumPerRoom,k[j])), 0);
					
					// m - sum_i x_ij <= (1 - kj) M
					cplex.addLe(cplex.sum(minimumPerRoom, cplex.negative(sumForKj)), 
							cplex.prod(bigM, cplex.sum(1, cplex.negative(k[j]))));
					
					// (m - 1) - sum_i x_ij >= kj (-M)
					cplex.addGe(cplex.sum(minimumPerRoom-1, cplex.negative(sumForKj)), cplex.prod(-bigM, k[j]));
					
				}
		
				
		
				//CONSTRAINTS ON VARIABLE x
				
				for(int i = 0; i < numParticipants; i++){
					IloLinearNumExpr sumOverJ = cplex.linearNumExpr();
					for(int j = 0; j < numOwners; j++){
						sumOverJ.addTerm(1, x[i][j]);
					}
					
					cplex.addLe(sumOverJ, 1);
					cplex.addGe(sumOverJ, 1);
				}
			
			
			

			// solve
			cplex.setParam(IloCplex.DoubleParam.TiLim, 7200);
			solved = cplex.solve();
			if (solved) {
				System.out.println("MIP first solved");
			} else {
				System.out.println("MIP first NOT solved");
			}
			
			

			// GET THE INFORMATION WE NEED

//			// passengers utilities
//			for (int i = 0; i < numParticipants; i++) {
//				IloLinearNumExpr passUtility = cplex.linearNumExpr();
//				for (int n = 0; n < numOwners; n++) {
//					passUtility.addTerm(v[i][n], x[i][n]);
//				}
//				passengersUtility[i] = cplex.getValue(passUtility);
//
//				System.out.println("student " + i + " " + passengersUtility[i] + " ");
//			}
//
//			// utility of the system
//			systemUtility = 0;
//
//			for (int i = 0; i < numParticipants; i++) {
//				systemUtility = systemUtility + passengersUtility[i];
//			}
//
//			System.out.println("systemUtility " + systemUtility);

//			// fairness of the solution
//			fairness = 0;
//			for (int i = 0; i < numParticipants; i++) {
//				for (int k = i + 1; k < numParticipants; k++) {
//					fairness = fairness + Math.abs(passengersUtility[i] - passengersUtility[k]);
//				}
//			}

			// allocation
			for (int i = 0; i < numParticipants; i++) {
				for (int j = 0; j < numOwners; j++) {
					allocation[i][j] = (int) cplex.getValue(x[i][j]);
					System.out.print(allocation[i][j] + " ");
				}
				System.out.println(" ");
			}

			// double[] temp = cplex.getValues(ut);
			// for( int i = 0; i < numParticipants; i++){
			// System.out.println("ut " + i + " " + temp[i]);
			// }

			cplex.end();

		} catch (IloException e) {
			e.printStackTrace();
		}
	}

//	public double getSystemUtility() {
//		return systemUtility;
//	}
//
//	public double getFairness() {
//		return fairness;
//	}
//
//	public double[] getPassengersUtility() {
//		return passengersUtility;
//	}

	public int[][] getAllocation() {
		return allocation;
	}

	public boolean getSolved() {
		return solved;
	}

}
