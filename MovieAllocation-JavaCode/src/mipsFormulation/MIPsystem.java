package mipsFormulation;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.DoubleParam;


public class MIPsystem {
	
	private double[][] v;
    
	double timeLimitInSecs;
    double systemUtility;
    double fairness;
    double allocatedPassengers;
    double[] passengersUtility;
    int[][] allocation;
    
    boolean solved;
    
    int numParticipants;
    int numOwners;
    int minimumUserWithSameRecommendation;
    IloCplex cplex;
    
    double bigM = 99999999;
	
	
	public MIPsystem(double[][] estimatedRatings, int numParticipants, int numOwners, int minimumUserWithSameRecommendation, IloCplex cplex, double timeLimitInSecs){
				
		this.cplex = cplex;
		this.numParticipants = numParticipants;
		this.numOwners = numOwners;
		this.minimumUserWithSameRecommendation = minimumUserWithSameRecommendation;
		
		passengersUtility = new double[numParticipants];
		allocation = new int[numParticipants][numOwners];
		
		this.timeLimitInSecs = timeLimitInSecs;
		
		v = estimatedRatings;

		program();
		
	}
	
	
	
	public void program(){
		 try{
			 
			 // instance of the engine TODO cleanModel
			 cplex.clearModel();
			 cplex.setParam(DoubleParam.TiLim, timeLimitInSecs);
			 //IloCplex cplex = new IloCplex();
			 
			 // variables --- allocation: x_i,j
			 // 0 no i in j, 1 i in j
			 IloNumVar[][] x = new IloNumVar[numParticipants][];
			 for(int i = 0; i < numParticipants; i++){
				 x[i] = cplex.boolVarArray(numOwners);
			 }
			  
			 // k variable
			 IloNumVar[] k = cplex.boolVarArray(numOwners);
			  
			 
			  
			  
			  // OBJECTIVE
			  IloLinearNumExpr obj = cplex.linearNumExpr();
			  
			  for(int i = 0; i < numParticipants; i++){
				  for(int n = 0; n < numOwners; n++){
					  obj.addTerm(v[i][n], x[i][n]);
				  }
			  }
			  
			  cplex.addMaximize(obj);
			  
			  
			// ROOMS constraints
			IloLinearNumExpr sumForKj;
			for(int j = 0; j < numOwners; j++){
				sumForKj = cplex.linearNumExpr();
				for(int i = 0; i < numParticipants; i++){
					sumForKj.addTerm(1, x[i][j]);
				}
				
				// sum_i x_ij >= m kj
				cplex.addGe(cplex.sum(sumForKj, cplex.prod(-minimumUserWithSameRecommendation,k[j])), 0);
				
				// m - sum_i x_ij <= (1 - kj) M
				cplex.addLe(cplex.sum(minimumUserWithSameRecommendation, cplex.negative(sumForKj)), 
						cplex.prod(bigM, cplex.sum(1, cplex.negative(k[j]))));
				
				// (m - 1) - sum_i x_ij >= kj (-M)
				cplex.addGe(cplex.sum(minimumUserWithSameRecommendation-1, cplex.negative(sumForKj)), cplex.prod(-bigM, k[j])); // TODO Seems redundant!
				
				// kj M >= sum_i x_i,j
				cplex.addGe(cplex.prod(bigM, k[j]), sumForKj);
				
			}
			
			//CONSTRAINTS ON VARIABLE x
			
			for(int i = 0; i < numParticipants; i++){
				IloLinearNumExpr sumOverJ = cplex.linearNumExpr();
				for(int j = 0; j < numOwners; j++){
					sumOverJ.addTerm(1, x[i][j]);
				}
				
				cplex.addLe(sumOverJ, 1);
				cplex.addGe(sumOverJ, 0.9);
			}
			
			
			
		    // solve
			// cplex.setParam(IloCplex.DoubleParam.TiLim, 7200);
			solved = cplex.solve();
		    if(solved){
		    	System.out.println("MIP system solved");
		    } else{
		    	System.out.println("MIP system NOT solved "  + solved);
		    }
		    
			
		    
		    
		    
			// GET THE INFORMATION WE NEED
			

		    // passengers utilities
			for(int i = 0; i < numParticipants; i++){
				IloLinearNumExpr passUtility = cplex.linearNumExpr();
				for(int n = 0; n < numOwners; n++){
					passUtility.addTerm(v[i][n], x[i][n]);
					System.out.println(cplex.getValue(x[i][n]));
				}
				passengersUtility[i] = cplex.getValue(passUtility);

				System.out.println("student " + i + " " + passengersUtility[i]  + " ");
			}
			
			
			// max utility of the system
		    systemUtility = 0;
		    
		    for(int i = 0; i < numParticipants; i++){
		    	systemUtility = systemUtility + passengersUtility[i];
		    }
		    
		    System.out.println("systemUtility " + systemUtility);
		    
		    
//		    // fairness of the solution
//		    fairness = 0;
//		    for(int i = 0; i < numParticipants; i++){
//		    	for(int k = i+1; k < numParticipants; k++){
//		    		fairness = fairness + Math.abs(passengersUtility[i]-passengersUtility[k]);
//		    	}
//		    }
		    
		    
		    // allocation
		    for (int i = 0; i < numParticipants; i++) {
		    	int proj = 100;
		    	int sum = 0;
		    	for (int j = 0; j < numOwners; j++) {
		    		proj = 0;
					allocation[i][j] = (int) Math.round(cplex.getValue(x[i][j]));
					//System.out.println(cplex.getValue(x[i][j]) + " cplex.getValue(x[i][j] ");
					//System.out.println((int) cplex.getValue(x[i][j]) + " (int) cplex.getValue(x[i][j] ");
					//System.out.print(allocation[i][j] + " ");
					sum = sum + allocation[i][j];
					if (allocation[i][j] >= 0.9){
						proj = j + 1;
						int stud = i + 1;
						System.out.println("participant " + stud + " " + passengersUtility[i]  + " " + "movie = " + proj);
					}
				}
		    	
		    	if(sum == 0.0){
		    		System.out.println("sum = " + sum);
		    	}

		    }

		    
		    
		    
		   //cplex.end(); 
		    
			 
		 }catch (IloException e){
			 e.printStackTrace();
		 }
	}
	
	public double getSystemUtility(){
		return systemUtility;
	}

//	public double getFairness(){
//		return fairness;
//	}
//	
//	public double[] getPassengersUtility(){
//		return passengersUtility;
//	}
	
	public int[][] getAllocation(){
		return allocation;
	}
	
	public boolean getSolved(){
		return solved;
	}
	

}
