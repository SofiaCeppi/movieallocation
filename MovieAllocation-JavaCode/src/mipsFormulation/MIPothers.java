package mipsFormulation;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import usersGeneration.User;

public class MIPothers {

	private double[][] v;
    
//    double systemUtility;
//    double fairness;
//    double allocatedPassengers;
//    double[] passengersUtility;
    int[][] allocation;
    
    boolean solved;
    
    private int numParticipants;
    private int numOwners;
    private int minimumPerRoom;
    private double bigM = 99999999;
	private double maxSystemUtility;
	private double levelSystemUtility;
	private ResultsOfARun ror;
	private int numSolutionsSoFar;
	private String noiseModel;
	private double epsilon;
	private double[] taxesValue;

	public MIPothers(double maxSystemUtility, double levelSystemUtility,
			double[][] estimatedRatings, int numUsers, int numMovies, 
			int minimumPerRoom, ResultsOfARun ror, int numSolutionsSoFar, String noiseModel,
			double epsilon) {

		// this.participantsList = participantsList;
		this.maxSystemUtility = maxSystemUtility;
		this.levelSystemUtility = levelSystemUtility;

		this.numParticipants = numUsers;
		this.numOwners = numMovies;
		this.minimumPerRoom = minimumPerRoom;
		this.ror = ror;
		this.numSolutionsSoFar = numSolutionsSoFar;
		this.noiseModel = noiseModel;
		this.epsilon = epsilon;

//		passengersUtility = new double[numParticipants];
		allocation = new int[numParticipants][numOwners];
		taxesValue = new double[numParticipants];

		v = estimatedRatings;

		program();

	}

	public void program() {
		try {

			// instance of the engine
			IloCplex cplex = new IloCplex();

			// variables --- allocation: x_i,j
			// 0 no i in j, 1 i in j
			IloIntVar[][] x = new IloIntVar[numParticipants][];
			for (int i = 0; i < numParticipants; i++) {
				x[i] = cplex.boolVarArray(numOwners);
			}

			 // k variable
			 IloNumVar[] k = cplex.boolVarArray(numOwners);
			 
			 // abs value
			 IloNumVar[][] abs = new IloNumVar[numParticipants][];
				for (int i = 0; i < numParticipants; i++) {
					abs[i] = cplex.numVarArray(numParticipants, 0, Double.MAX_VALUE);
				}
				
				// taxes variable
				 IloNumVar[] taxes = cplex.numVarArray(numOwners, 0.0, Double.MAX_VALUE);
				 
				// variable dummy
				  IloNumVar dummy = cplex.numVar(1, 1);
				


				// OBJECTIVE
				 
				 double[] firstPassengersUtility = new double[numParticipants];
				 Solution firstSolution = ror.getFirstSolution();
				  int[][] firstAllocation = firstSolution.getAllocation();
				  double[][] ratings = firstSolution.getEstimatedRatings();
				  
				  for(int i= 0; i<numParticipants; i++){
					  for(int j = 0; j < numOwners; j++){
						  firstPassengersUtility[i] = firstPassengersUtility[i] + firstAllocation[i][j] * ratings[i][j];
					  }
				  }
				 
				 
				 
			IloLinearNumExpr obj = cplex.linearNumExpr();
			for (int i = 0; i < numParticipants; i++) {
				for (int h = i + 1; h < numParticipants; h++) {
					obj.addTerm(1, abs[i][h]);
				}
			}
			
			if(noiseModel.equals("CONSTANT")){
				
				for(int i = 0; i < numParticipants; i++){

					obj.addTerm(bigM*firstPassengersUtility[i], dummy);
					obj.addTerm(-epsilon*bigM, dummy);
					for(int j = 0; j < numOwners; j++){
						obj.addTerm(-bigM*v[i][j], x[i][j]);
					}
					obj.addTerm(bigM, taxes[i]);
					
				}
				
			}else if(noiseModel.equals("LOGIT")){
				
//				obj.addTerm(arg0, arg1);
				
			}else{
				System.out.println("Noise Model Not Recognised");
				
			}

				cplex.addMinimize(obj);
				
				
						
				// CONSTRAINT TO COMPUTE THE ABS VALUE
				for (int i = 0; i < numParticipants; i++) {

					IloLinearNumExpr utilI = cplex.linearNumExpr();
					for (int n = 0; n < numOwners; n++) {
						utilI.addTerm(v[i][n], x[i][n]);
					}
					utilI.addTerm(-1, taxes[i]);

					for (int h = 0; h < numParticipants; h++) {

						IloLinearNumExpr utilK = cplex.linearNumExpr();

						for (int n = 0; n < numOwners; n++) {
							utilK.addTerm(v[h][n], x[h][n]);
						}
						
						utilK.addTerm(-1, taxes[h]);

						cplex.addGe(cplex.sum(abs[i][h],
								cplex.negative(cplex.abs(cplex.sum(utilI, cplex.negative(utilK))))), 0.0);
						cplex.addLe(cplex.sum(abs[i][h],
								cplex.negative(cplex.abs(cplex.sum(utilI, cplex.negative(utilK))))), 0.0);
					}
				}


				// CONSTRAINT: LEVEL OF SYSTEM UTILITY
				IloLinearNumExpr sysUtil = cplex.linearNumExpr();
				// add social welfare to objective (weight w[0])
				for (int i = 0; i < numParticipants; i++) {
					for (int n = 0; n < numOwners; n++) {
						sysUtil.addTerm(v[i][n], x[i][n]);
					}
					sysUtil.addTerm(-1, taxes[i]);
				}

				System.out.println("maxSysUtil " + maxSystemUtility);
				System.out.println("levelSystemUtility " + levelSystemUtility);

				cplex.addGe(sysUtil, maxSystemUtility * levelSystemUtility);
			
			
				// ROOMS constraints
				IloLinearNumExpr sumForKj;
				for(int j = 0; j < numOwners; j++){
					sumForKj = cplex.linearNumExpr();
					for(int i = 0; i < numParticipants; i++){
						sumForKj.addTerm(1, x[i][j]);
					}
					
					// sum_i x_ij >= m kj
					cplex.addGe(cplex.sum(sumForKj, cplex.prod(-minimumPerRoom,k[j])), 0);
					
					// m - sum_i x_ij <= (1 - kj) M
					cplex.addLe(cplex.sum(minimumPerRoom, cplex.negative(sumForKj)), 
							cplex.prod(bigM, cplex.sum(1, cplex.negative(k[j]))));
					
					// (m - 1) - sum_i x_ij >= kj (-M)
					cplex.addGe(cplex.sum(minimumPerRoom-1, cplex.negative(sumForKj)), cplex.prod(-bigM, k[j]));
					
				}
		
				
				//CONSTRAINTS ON VARIABLE x
				
				for(int i = 0; i < numParticipants; i++){
					IloLinearNumExpr sumOverJ = cplex.linearNumExpr();
					for(int j = 0; j < numOwners; j++){
						sumOverJ.addTerm(1, x[i][j]);
					}
					
					cplex.addLe(sumOverJ, 1);
					cplex.addGe(sumOverJ, 1);
				}
			
			
				


			
			// CONSTRAINTS DIFFERENT SOLUTIONS
			
			// CONSTRAINT: GLOBAL SOLUTION DIFFERENT FROM THE PREVIOUS ONES
			  
			  Solution current = ror.getFirstSolution();
			  
			  int[][] previousAllocation;
			  
			  for(int s = 0; s < numSolutionsSoFar; s++){
				  
				  previousAllocation = current.getAllocation();
				  
				  IloLinearNumExpr allocationComparison = cplex.linearNumExpr();
				  
				  for(int i = 0; i < numParticipants; i++){
					  for(int j = 0; j < numOwners; j++){
						  allocationComparison.addTerm(1,(IloNumVar) cplex.abs
								  (cplex.sum(-previousAllocation[i][j], x[i][j])));
					  }
				  }
				  
				  cplex.addGe(allocationComparison, 1.0);
				  
					  current = current.getNext();
			  }
			  
			  
			  
			  // CONSTRAINT: MINIMUM TAXATION
			  
			  
			  
			  IloLinearNumExpr utilI = cplex.linearNumExpr();;
			  
			  if(noiseModel.equals("constant")){
				  
				 for(int i = 0; i < numParticipants; i++){
					 if(firstPassengersUtility[i] > 0.0){
						 

						 for(int n = 0; n < numOwners; n++){
								  utilI.addTerm(v[i][n], x[i][n]);
					     }
				      
				      utilI.addTerm(-1, taxes[i]);
				      
				      cplex.addLe(utilI, firstPassengersUtility[i] - epsilon);
					 }

				 }
				  
			  } //else { //if (noiseModel.equals("logit")){
//				  
//				  for(int i = 0; i < numPassengers; i++){
//					  
//					  if(firstPassengersUtility[i] > 0.0){
//					  
//					  utilI = cplex.linearNumExpr();
//					  
//					  for(int n = 0; n < numIntervalsAlpha; n++){
//				    	  utilI.addTerm(alpha[i][n], dummy);
//				    	  utilI.addTerm(-alpha[i][n], k_pickup[i][n]);
//				      }
//					  for(int n = 0; n < numIntervalsBeta; n++){
//				    	  utilI.addTerm(beta[i][n], dummy);
//				    	  utilI.addTerm(-beta[i][n], k_dropoff[i][n]);
//				      }
//				      
//				      utilI.addTerm(-1, t[i]);
//				      
//				      double utilityPreviousSolutionI = sumOfUserPreviousUtility(i);
//				      
//				      cplex.addLe(cplex.prod(epsilon, utilI), utilityBest[i] - epsilon * utilityPreviousSolutionI);
//					  }
//				      
//				  }
//				  
//			  }
			
			
			
			

			// solve
			cplex.setParam(IloCplex.DoubleParam.TiLim, 7200);
			solved = cplex.solve();
			if (solved) {
				System.out.println("MIP first solved");
			} else {
				System.out.println("MIP first NOT solved");
			}

			// GET THE INFORMATION WE NEED

//			// passengers utilities
//			for (int i = 0; i < numParticipants; i++) {
//				IloLinearNumExpr passUtility = cplex.linearNumExpr();
//				for (int n = 0; n < numOwners; n++) {
//					passUtility.addTerm(v[i][n], x[i][n]);
//				}
//				passengersUtility[i] = cplex.getValue(passUtility);
//
//				System.out.println("student " + i + " " + passengersUtility[i] + " ");
//			}
//
//			// utility of the system
//			systemUtility = 0;
//
//			for (int i = 0; i < numParticipants; i++) {
//				systemUtility = systemUtility + passengersUtility[i];
//			}
//
//			System.out.println("systemUtility " + systemUtility);
//
//			// fairness of the solution
//			fairness = 0;
//			for (int i = 0; i < numParticipants; i++) {
//				for (int k = i + 1; k < numParticipants; k++) {
//					fairness = fairness + Math.abs(passengersUtility[i] - passengersUtility[k]);
//				}
//			}

			// allocation
			for (int i = 0; i < numParticipants; i++) {
				for (int j = 0; j < numOwners; j++) {
					allocation[i][j] = (int) cplex.getValue(x[i][j]);
					System.out.print(allocation[i][j] + " ");
				}
				taxesValue[i] = cplex.getValue(taxes[i]);
			}

			// double[] temp = cplex.getValues(ut);
			// for( int i = 0; i < numParticipants; i++){
			// System.out.println("ut " + i + " " + temp[i]);
			// }

			cplex.end();

		} catch (IloException e) {
			e.printStackTrace();
		}
	}

//	public double getSystemUtility() {
//		return systemUtility;
//	}
//
//	public double getFairness() {
//		return fairness;
//	}
//
//	public double[] getPassengersUtility() {
//		return passengersUtility;
//	}

	public int[][] getAllocation() {
		return allocation;
	}
	
	public double[] getTaxes() {
		return taxesValue;
	}

	public boolean getSolved() {
		return solved;
	}

}

