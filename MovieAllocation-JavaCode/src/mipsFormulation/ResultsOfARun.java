package mipsFormulation;

public class ResultsOfARun {
	
	private Solution bestForSystem;
	private Solution firstSoltuion;
	
	
	ResultsOfARun  next;
	
	public ResultsOfARun(){
	}
	
	public void setBestForSystem(Solution best){
		bestForSystem = best;
	}
	
	public void setFirstSolution(Solution first){
		firstSoltuion = first;
	}
	
	public Solution getBestForSystem(){
		return bestForSystem;
	}
	
	public Solution getFirstSolution(){
		return firstSoltuion;
	}
	
	public ResultsOfARun getNext(){
		return next;
	}
	
	public void setNext(){
		next = new ResultsOfARun();
	}

}
