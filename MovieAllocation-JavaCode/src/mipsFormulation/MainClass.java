package mipsFormulation;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import inferenceFromUserActions.*;
import inferenceFromUserActions.utilityFunctions.*;
import inferenceFromUserActions.primitiveData.*;
import inferenceFromUserActions.interactions.*;
//import inferenceFromUserActions.factorGraphGenerators.*;
import inferenceFromUserActions.beliefFunctions.*;
import inferenceFromUserActions.loopyBeliefPropagation.*;
import simulation.SimulateBehaviour;
import usersGeneration.Ratings;
import utilities.ArrayMethods;

public class MainClass {
	
	static int experimentAllvsCoarse;
	
	static int numOwners;
	
	static int[] usedHeuristic;
	
	static int numTotalParticipants;
	static int numTotalOwners;
	static Ratings gtRatingsClass;
	static int numParameters;
	static int[] cardinalityPerParameter;
	static int[] positionParticipants;
	static int[] positionOwners;
	static Integer[][] itemsMatrix;
	private static ArrayList<DiscreteAssignment> allAssignments;
	private static ArrayList<SumOfConditionalGaussians> utilityFunction;
	private static String reality;
	private static String folder;
	private static String folderBis;
	
	private static final String COMMA_DELIMITER = ",";	
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static int[] seedDays;
	private static int[] seedSelectionDays;
	private static int[][] usersDays;
	private static int[][] moviesDays;
	private static long[] timeUpdate;
	private static long[] timeMILPS;
//	private static double[][] dayInfo;

	

	public static void main(String[] args){
		
		int seed;
		int seedSelection;
		int outer_seedSelection;
			
		for	( int realityIndex = 0; realityIndex < 1; realityIndex++ ) {			
			//parameters that we need to set manually
			reality = String.format("%03d", realityIndex); // "000";
			
			// either "/genome/" OR "/coarse/" TODO
			// SOFIA
			// folder = "/Users/sofiaceppi/Documents/git/movieallocationData/" + reality + "/coarse/";
			// PAVLOS 
			folder = "/Users/Cortu01/Documents/movieallocation/movieallocationData/" + reality + "/coarse/";
			
			new File(folder + "/output/").mkdir();
			System.out.println("Reality: " + reality);
		
			for ( int outer_seed = 0 ; outer_seed < 1; outer_seed++ ) {
		
				String seed_text = String.format("%03d", outer_seed); // "000";
				System.out.println("Experiment Instance: " + seed_text);
				
				outer_seedSelection = outer_seed; // 0;
		
				// either "/genome/" OR "/coarse/" TODO
				// SOFIA
				// folderBis = "/Users/sofiaceppi/Documents/git/movieallocationData/" + reality + "/coarse/output/" + seed_text + "/";
				// PAVLOS
				folderBis = "/Users/Cortu01/Documents/movieallocation/movieallocationData/" + reality + "/coarse/output/" + seed_text + "/";
	     
				new File(folderBis).mkdir();
			
				double timeLimitInSecs = 30;
	     
				int numDays = 20;
		
				experimentAllvsCoarse = 2; // 0 -> no, any; 1 -> yes, genome; 2 -> yes, coarse
				
				usedHeuristic = new int[numDays];
				
				int numParticipants = 21; // 12; // 20; // My MIP start solution works only for numParticipants in the multiples of minimumUserWithSameRecommendation. Should decide on the size of 'groups' beforehand and use that to say that an owner is 'used up'.
				numOwners = 15 + outer_seed;// 20; // 6; // 20; // was 10 // Max in itemDescriptions.csv -> cell A1 = numTotalOwners
		
				String noiseModel = "CONSTANT"; // TODO
				double epsilon = 0.05;
				double alphaProbabilityConstant = 0.7;
		
				int numRecommendations = 5; // 5; // TODO
		
				int minimumPerRoom = 5; // 5;
				int minimumUserWithSameRecommendation = (int) Math.ceil(minimumPerRoom + minimumPerRoom * (1 - alphaProbabilityConstant));
		
				// either "SocialWelfare" or "Fairness"
				// but "Fairness" is not completely implemented
				String experiment = "SocialWelfare"; 
		
				// either "OUR" or "NOTOUR"
				String model = "OUR";

		
				String itemPath = folder + "itemDescriptions.csv";
				String gtPath = folder + "groundTruth.csv";
		
				//set the itemMatrix, numParameters, cardinality
				setItemMatrix(itemPath);
				
				if ( experimentAllvsCoarse == 1 ) {
					numOwners = numTotalOwners;
				}
								
				seedDays = new int[numDays];
				seedSelectionDays = new int[numDays];
				usersDays = new int[numParticipants][numDays];
				moviesDays = new int[numTotalOwners][numDays];//numOwners
				timeUpdate = new long[numDays];
				timeMILPS = new long[numDays];
		
				String fileWithMEANs = folder + "priorMEANs.csv";
				String fileWithVARs = folder + "priorVARs.csv";
				
				//set the GT Matrix, numTotalParticipants, numTotalOwners
				setGroundTruthMatrix(gtPath);

				ArrayList<Parameter> parametersList = new ArrayList<>();
				Parameter par;
		
				for(int v = 0; v < numParameters; v++){
					par = new Parameter("var"+v, "int", true, "notSpecified");
					parametersList.add(par);
				}
		
				Domain domain = new Domain(parametersList);

				Parameter utility = new Parameter("Utility", "double", false, "Real");
		
				utilityFunction = new ArrayList<>();
				for(int i = 0; i < numTotalParticipants; i++){
					//utilityFunction.add(new SumOfConditionalGaussians(numParameters, cardinalityPerParameter, utility, domain ));
					utilityFunction.add( new SumOfConditionalGaussians( numParameters, cardinalityPerParameter, utility, domain, fileWithMEANs, fileWithVARs, i ) ); 
				}
		
				trackReality(reality, numDays, numParticipants, numOwners, noiseModel, epsilon,
						alphaProbabilityConstant, numRecommendations, minimumPerRoom, minimumUserWithSameRecommendation, experiment);
		
				//for every day
				MipsCaller mc = new MipsCaller();
				IloCplex cplex = null;
				IloCplex cplexOther = null;
				try {
					cplex = new IloCplex();
					cplexOther = new IloCplex();
				} catch (IloException e) {
					e.printStackTrace();
				}
		
				for(int day = 0; day < numDays; day++){
			
//			dayInfo = new double [6][numParticipants * numRecommendations];
					seed = outer_seed + 1;
					seedSelection = outer_seedSelection + 1;
		
					dailySelection(seed, numParticipants);//, numOwners);
						
					double[][] expectedRatings = new double[numParticipants][numOwners];
					
					allAssignments = new ArrayList<>();
					DiscreteAssignment assignment;
					List<Object> values;// = new ArrayList<>();
					for(int j = 0; j < numOwners; j++){
						
						values = new ArrayList<>();
						
						for(int p = 0; p < numParameters; p++){
							values.add(itemsMatrix[positionOwners[j]][p]);
						}
			
						assignment = new DiscreteAssignment(domain, values);
						allAssignments.add(assignment);
			
						for(int i = 0; i < numParticipants; i++){
				
							System.out.println("ID part " + positionParticipants[i]);
							System.out.println(utilityFunction.size());
				
							double temp = 1000* (utilityFunction.get(positionParticipants[i])).getExpectedUtility(assignment);
							temp = temp / 1000;
//							if ( temp < 0 ) {
//								temp = 0;
//							}
							//TODO delete comment
							temp = temp + 1.5;
							expectedRatings[i][j] = temp; // Math.round(a *1000)/1000; //TODO Added third decimal rounding, Math.round seems to change the output to int!
						}
					}
		
					Ratings estimatedRatingsClass = new Ratings(numParticipants, numOwners);
					estimatedRatingsClass.setRatings(expectedRatings);
		
					//MipsCaller mc = new MipsCaller();
					ResultsOfARun ror = mc.callMips(estimatedRatingsClass, gtRatingsClass, numParticipants, numOwners, 
							noiseModel, numRecommendations, epsilon, experiment, minimumUserWithSameRecommendation, model, 
							cplex, cplexOther, timeLimitInSecs);
		
					// WAIT FOR TERMINATION?
		
					int[][] temp = ror.getFirstSolution().getAllocation();
					
					for(int i = 0; i < numParticipants; i++){
						for(int j = 0; j < numOwners; j++){
							System.out.print(temp[i][j]);
						}
						System.out.println();
					}
		
					// note that taxes are in ror
					SimulateBehaviour sb = new SimulateBehaviour(ror, numParticipants, numOwners, 
							numRecommendations, gtRatingsClass, positionOwners, allAssignments);
		
					ArrayList<Selection> usersSelection = sb.selectionNoise(alphaProbabilityConstant, noiseModel, seedSelection);

					// userSelection is a list of selection, one for each user
					// a user selection contains a list of discrete assignment
		
	
					long lStartTime = new Date().getTime();
				
					ArrayList<Selection> PairwiseSelectionForOneUser;
					for(int i = 0; i < numParticipants; i++){
						
						PairwiseSelectionForOneUser = (usersSelection.get(i)).getPairwiseComparisons();
			
						for(Selection pair : PairwiseSelectionForOneUser){

							PrepareForLBP_fromSumOf1D_CG_pairwiseComparison problemSpecification = 
									new PrepareForLBP_fromSumOf1D_CG_pairwiseComparison(
											(utilityFunction.get(positionParticipants[i])), pair);
		
							JSkillsIntegrator JSkills = new JSkillsIntegrator( problemSpecification );
							JSkills.run();
			
							List<ProbabilityDensityFunction> potentials = JSkills.getPotentials();			
							problemSpecification.savePosterior(potentials);

							// Normalising here -> Hard-coded max utility
							utilityFunction.set(positionParticipants[i] , (SumOfConditionalGaussians) problemSpecification.getFullPosterior() );
							utilityFunction.get(positionParticipants[i]).normalise( 5 );
						}
					}
		
					long lEndTime = new Date().getTime();

					long difference = lEndTime - lStartTime;
		
					int[] t = sb.getSelection();
					for( int tt = 0; tt < numParticipants; tt++ ) {
						System.out.println(t[tt]);
					}
		
					trackDay(sb, numParticipants, numRecommendations, expectedRatings, minimumPerRoom, day);
		
					seedDays[day] = seed;
					seedSelectionDays[day] = seedSelection;
		
					for( int i = 0; i < numParticipants; i++ ){
						usersDays[i][day] = positionParticipants[i];
					}
					
					for(int j = 0; j < numOwners; j++){
						moviesDays[j][day] = positionOwners[j];
					}
		
					timeUpdate[day] = difference;
					timeMILPS[day] = mc.getDifference();
		
				}// end for the day
				
				cplex.end();
				cplexOther.end();
		
				trackRealityDays(seedDays, seedSelectionDays, usersDays, moviesDays, numDays,
				numParticipants, numOwners, timeUpdate, timeMILPS);
		
			} // for outer seed (i.e. experiment instance)		
			
		} // for reality
			
	}// end of main
	
	
	
	// set the itemsMatrix with all the items, numTotalOwners, and numParameters
	static public void setItemMatrix(String path){
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(path));
		
		String[] itemInfo = br.readLine().split(",");
		
		numTotalOwners = Integer.parseInt(itemInfo[0]);
		numParameters = Integer.parseInt(itemInfo[1]);
		int cardinality = Integer.parseInt(itemInfo[2]);
		
		cardinalityPerParameter = new int[numParameters];
		
		for(int p = 0; p <numParameters; p++){
			cardinalityPerParameter[p] = cardinality;
		}
		
		itemsMatrix = new Integer[numTotalOwners][numParameters];
		
		for(int item = 0; item < numTotalOwners; item++){
			itemInfo = br.readLine().split(",");
			for(int par = 0 ; par < numParameters; par++){
				itemsMatrix[item][par] = Integer.parseInt(itemInfo[par].replaceAll("\\W", ""));
			}
		}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
		}
		
		
	}
	
	
	// set the gtMatrix with all the users and items and numTotalParticipants
	static public void setGroundTruthMatrix(String path){
		
		BufferedReader br = null;
		double[][] gtMatrix = null;
		try {
			br = new BufferedReader(new FileReader(path));
		
		String[] gtInfo = br.readLine().split(",");
		
		numTotalParticipants = Integer.parseInt(gtInfo[0]);
		
		gtMatrix = new double[numTotalParticipants][numTotalOwners];
		
		for(int user = 0; user < numTotalParticipants; user++){
			gtInfo = br.readLine().split(",");
			for(int item = 0 ; item < numTotalOwners; item++){
				gtMatrix[user][item] = Double.parseDouble(gtInfo[item]);
			}
		}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
		}

		
		gtRatingsClass = new Ratings(numTotalParticipants, numTotalOwners);
		gtRatingsClass.setRatings(gtMatrix);
	}
	
	
	static public void dailySelection(int seed, int numParticipants ) {//, int numOwners){
		
		// random selection of the required number of users and items
		
		Random randomGenerator = new Random(seed); 
		
		// array of position of participants in the original file
		positionParticipants = new int[numParticipants];
		for(int i = 0; i < numParticipants; i++){
			int position = randomGenerator.nextInt(numTotalParticipants);
			boolean alreadySelected = false;
			for(int n = 0; n < i; n++){
				if (positionParticipants[n] == position){
					alreadySelected = true;
					i--;
				}
			}
			if(alreadySelected == false){
				positionParticipants[i] = position;
			}
		}
		
		//TODO: not sure this will work well for when all the users need to be taken into account
		// array of position of genomes/coarse in the original file
		if ( experimentAllvsCoarse != 2 ) {
			
			positionOwners = new int[numOwners];
			for (int i = 0; i < numOwners; i++) {
				int position = randomGenerator.nextInt(numTotalOwners);
				boolean alreadySelected = false;
				for (int n = 0; n < i; n++) {
					if (positionOwners[n] == position) {
						alreadySelected = true;
						i--;
					}
				}
				if (alreadySelected == false) {
					positionOwners[i] = position;
				}
			}
			
		} else {
			
			int numMaxExamplesPerClass = 1;
			int minNumOwnersNeeded = 15;
			int numOwnersPicked = 0;
			int itemClass;
			int numClasses = cardinalityPerParameter[0];
			int[] count = new int[numClasses];
			boolean[] ITEM_WAS_ADDED = new boolean[numTotalOwners];
			
			boolean NOT_HAD_ENOUGH = true;
			
			while (NOT_HAD_ENOUGH) {
				
				for (int i = 0; i < numTotalOwners; i++) {
					itemClass = itemsMatrix[i][0];
					if (count[itemClass] < numMaxExamplesPerClass && !ITEM_WAS_ADDED[i] ) {
						count[itemClass]++;
						ITEM_WAS_ADDED[i] = true;
					}
				}

				numOwnersPicked = ArrayMethods.sumElements(count);			
				if ( numOwnersPicked < minNumOwnersNeeded ) {
					numMaxExamplesPerClass++;
				} else {
					NOT_HAD_ENOUGH = false;
				}
				
			}
			
			numOwners = numOwnersPicked;
			
			positionOwners = new int[numOwners];
			for (int i = 0; i < numOwners; i++) {
				int position = randomGenerator.nextInt(numTotalOwners);
				boolean alreadySelected = false;
				
				if ( !ITEM_WAS_ADDED[position] ) {
					alreadySelected = true;
					i--;
				} else {
				
					for (int n = 0; n < i; n++) {
						if (positionOwners[n] == position) {
							alreadySelected = true;
							i--;
						}
					}
				
				}
				if (alreadySelected == false) {
					positionOwners[i] = position;
				}
			}
			
			
/*			
			
			int theClassIsRepresented = 0;
			int n_CoarseClasses = cardinalityPerParameter[0];
			int[] classIsRepresented = new int[n_CoarseClasses];
			for ( int i = 0; i < n_CoarseClasses; i++ ) {
				theClassIsRepresented = 0;
				if ( theClassIsRepresented == 1 ) {
					classIsRepresented[i] = 1;
				}
			}
			
			numOwners = ArrayMethods.sumElements(classIsRepresented);
			positionOwners = new int[numOwners]; // only smaller, cause not all classes will be represented
			
			for (int i = 0; i < numOwners; i++) {
				int position = randomGenerator.nextInt(numTotalOwners);
				
				
				
			}*/
			
		}
		
	}
	
	
	public static void trackReality(String folder, int numDays, int numParticipants, 
			int numOwners, String noiseModel, double epsilon,
			double alphaProbabilityConstant, int numRecommendations, 
			int minimumPerRoom, int minimumUserWithSameRecommendation, String experiment) {

		FileWriter fileWriter = null;

		try {
			fileWriter = new FileWriter(folderBis + "reality.csv");

			fileWriter.append(String.valueOf(numDays));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(numParticipants));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(numOwners));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(noiseModel);
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(epsilon));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(alphaProbabilityConstant));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(numRecommendations));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(minimumPerRoom));
			fileWriter.append(NEW_LINE_SEPARATOR); 
			fileWriter.append(String.valueOf(minimumUserWithSameRecommendation));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(experiment);
			fileWriter.append(NEW_LINE_SEPARATOR);


		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

	}
	
	
	public static void trackRealityDays(int[] seedDays, int []seedSelectionDays, 
			int [][] usersDays, int [][] moviesDays, int numDays,
			int numParticipants, int numOwners, long[] timeUpdate, long[] timeMILPS){
		
		FileWriter fileWriter = null;

		try {
			fileWriter = new FileWriter(folderBis + "realityDays.csv");

			//write seeds
			fileWriter.append(String.valueOf(seedDays[0]));
			for(int d = 1; d < numDays; d++){
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(seedDays[d]));
			}
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			//write seedSelections
			fileWriter.append(String.valueOf(seedSelectionDays[0]));
			for(int d = 1; d < numDays; d++){
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(seedSelectionDays[d]));
			}
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			//write participant for every day
			for(int i = 0; i < numParticipants; i++){
				fileWriter.append(String.valueOf(usersDays[i][0]));
				for(int d = 1; d < numDays; d++){
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(usersDays[i][d]));
				}
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			
			//write movie for every day
			for(int j = 0; j < numOwners; j++){
				fileWriter.append(String.valueOf(moviesDays[j][0]));
				for(int d = 1; d < numDays; d++){
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(moviesDays[j][d]));
				}
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			
			//write timeUpdate
			fileWriter.append(String.valueOf(timeUpdate[0]));
			for(int d = 1; d < numDays; d++){
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(timeUpdate[d]));
			}
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			//write timeMILPS
			fileWriter.append(String.valueOf(timeMILPS[0]));
			for(int d = 1; d < numDays; d++){
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(timeMILPS[d]));
			}
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
		
	}

	
	public static void trackDay(SimulateBehaviour sb,
			int numParticipants, int numRecommendations,
			double[][] expectedRatings, int minimumPerRoom, int day){
		
		FileWriter fileWriter = null;

		try {
			fileWriter = new FileWriter(folderBis + "day" + day + ".csv");
			
			//movies ID
			int[][] recommendedMovies = sb.getListRecommendations();
			for(int i = 0; i < numParticipants; i++){
				for(int r = 0; r < numRecommendations; r++){
					fileWriter.append(String.valueOf(positionOwners[recommendedMovies[i][r]]));
					if(r == numRecommendations - 1 && i == numParticipants -1){
						fileWriter.append(NEW_LINE_SEPARATOR);
					} else{
						fileWriter.append(COMMA_DELIMITER);
					}
				}
			}
			
			//estimated utility
			for(int i = 0; i < numParticipants; i++){
				for(int r = 0; r < numRecommendations; r++){
					fileWriter.append(String.valueOf(expectedRatings[i][recommendedMovies[i][r]]));
					if(r == numRecommendations - 1 && i == numParticipants -1){
						fileWriter.append(NEW_LINE_SEPARATOR);
					} else{
						fileWriter.append(COMMA_DELIMITER);
					}
				}
			}
			
			//taxation
			double[][] taxes = sb.getAllTaxes();
			for(int i = 0; i < numParticipants; i++){
				for(int r = 0; r < numRecommendations; r++){
					fileWriter.append(String.valueOf(taxes[i][r]));
					if(r == numRecommendations - 1 && i == numParticipants -1){
						fileWriter.append(NEW_LINE_SEPARATOR);
					} else{
						fileWriter.append(COMMA_DELIMITER);
					}
				}
			}
			
			//groundtruth
			double[][] gt = sb.getGt();
			for(int i = 0; i < numParticipants; i++){
				for(int r = 0; r < numRecommendations; r++){
					fileWriter.append(String.valueOf(gt[i][r]));
					if(r == numRecommendations - 1 && i == numParticipants -1){
						fileWriter.append(NEW_LINE_SEPARATOR);
					} else{
						fileWriter.append(COMMA_DELIMITER);
					}
				}
			}
			
			
			
			//selections 0 1
			int[] selection = sb.getSelection(); //item in the list of current movies
			for(int i = 0; i < numParticipants; i++){
				System.out.println(selection[i]);
				for(int r = 0; r < numRecommendations; r++){
					if(r == selection[i]){
						fileWriter.append("1");
					} else {
						fileWriter.append("0");
					}
					if(r == numRecommendations - 1 && i == numParticipants -1){
						fileWriter.append(NEW_LINE_SEPARATOR);
					} else{
						fileWriter.append(COMMA_DELIMITER);
					}
				}
			}
			
			//do they watch the movie? 0 1
			for(int i = 0; i < numParticipants; i++){
				
				System.out.println("user " + i + " selection[i] " + selection[i]);
				
				int count = 0;
				for(int ii = 0; ii < numParticipants; ii++){
					System.out.println("user " + ii + " selection[ii] " + selection[ii]);
					if(selection[i] == selection[ii]){
						count = count + 1;
						System.out.println("count " + count);
					}
				}
				
				for(int r = 0; r < numRecommendations; r++){
					
					System.out.println("selection[i] " + selection[i] + " count " + count);
					
					if(r == selection[i] && count >= minimumPerRoom){
						fileWriter.append("1");
					} else {
						fileWriter.append("0");
					}
					if(r == numRecommendations - 1 && i == numParticipants -1){
						fileWriter.append(NEW_LINE_SEPARATOR);
					} else{
						fileWriter.append(COMMA_DELIMITER);
					}
				}
			}


		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}	
		
	}

}

