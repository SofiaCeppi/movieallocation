package mipsFormulation;

public class Solution {

	private double[][] estimatedRatings;
	private double[][] gtRatings;
	private int[][] allocation;
	private double[] tax;
	private double levelSystemUtility;
	private Solution next = null;
	private String noiseModel;
	private double epsilon;
	private int minimumPerRoom;
	private int numRecommendations;

	public Solution(){
		
	}
	
	public Solution(double[][] estimatedRatings, double[][] gtRatings, int[][] allocation, 
			double[] tax, double levelSystemUtility,
			String noiseModel, double epsilon, int minimumPerRoom, int numRecommendations){
		
		this.setEstimatedRatings(estimatedRatings);
		this.setGtRatings(gtRatings);
		this.setAllocation(allocation);
		this.setTax(tax);
		this.setNoiseModel(noiseModel);
		this.setEpsilon(epsilon);
		this.setMinimumPerRoom(minimumPerRoom);
		this.setNumRecommendations(numRecommendations);
	}

	
	
	public double[][] getEstimatedRatings() {
		return estimatedRatings;
	}

	public void setEstimatedRatings(double[][] estimatedRatings) {
		this.estimatedRatings = estimatedRatings;
	}

	public double[][] getGtRatings() {
		return gtRatings;
	}

	public void setGtRatings(double[][] gtRatings) {
		this.gtRatings = gtRatings;
	}

	public int[][] getAllocation() {
		return allocation;
	}

	public void setAllocation(int[][] allocation) {
		this.allocation = allocation;
	}

	public double[] getTax() {
		return tax;
	}

	public void setTax(double[] tax) {
		this.tax = tax;
	}

	public double getLevelSystemUtility() {
		return levelSystemUtility;
	}

	public void setLevelSystemUtility(double levelSystemUtility) {
		this.levelSystemUtility = levelSystemUtility;
	}

	public Solution getNext() {
		return next;
	}

	public void setNext(Solution next) {
		this.next = next;
	}

	public String getNoiseModel() {
		return noiseModel;
	}

	public void setNoiseModel(String noiseModel) {
		this.noiseModel = noiseModel;
	}

	public double getEpsilon() {
		return epsilon;
	}

	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	public int getMinimumPerRoom() {
		return minimumPerRoom;
	}

	public void setMinimumPerRoom(int minimumPerRoom) {
		this.minimumPerRoom = minimumPerRoom;
	}

	public int getNumRecommendations() {
		return numRecommendations;
	}

	public void setNumRecommendations(int numRecommendations) {
		this.numRecommendations = numRecommendations;
	}
	
	
	


}
