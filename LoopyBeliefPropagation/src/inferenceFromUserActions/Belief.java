package inferenceFromUserActions;

import java.util.List;

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.primitiveData.Domain;

/**
 * This class is not used.
 * 
 */

/**
 * @author Cortu01
 *
 */
public class Belief {

	private Domain domain;
	private ProbabilityDensityFunction density;//Can be empty if this isn't grounded; 'ProbabilityDensityFunction' is an abstract class.
	private List<Belief> subBelief;
	private Boolean isGrounded;//Indicates that this belief does not branch out to other sub-beliefs, and therefore 'subBelief' must be empty.
	
	/**
	 Empty Constructor:
	 */
	public Belief( ) {
		
	}
	/**
	 Full Constructor: 
	 */
	public Belief( Domain domain, List<Belief> subBelief, ProbabilityDensityFunction density, Boolean isGrounded ) {
		// Instantiating class variables:
		try {
			if ( isGrounded && subBelief == null || !subBelief.isEmpty() ) {
				throw new IllegalArgumentException(">>> Error while creating Belief: expected empty 'List<Belief> "
						+ "subBelief' because the Belief was declared as grounded. Setting 'Boolean isGrounded' to "
						+ "'false'");
			}
			else {
			}
		} catch ( IllegalArgumentException e ) {
			isGrounded = false;
		}
		this.domain = domain;
		this.subBelief = subBelief;
		this.density = density;
		this.isGrounded = isGrounded;
		
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public ProbabilityDensityFunction getProbabilityDensityFunction() {
		return density;
	}

	public void setDistribution(ProbabilityDensityFunction density) {
		this.density = density;
	}

	public List<Belief> getSubBelief() {
		return subBelief;
	}

	public void setSubBelief(List<Belief> subBelief) {
		this.subBelief = subBelief;
		if ( subBelief.isEmpty() ) {
			this.isGrounded = true;
		} else {
			this.isGrounded = false;
		}
			
	}

	public Boolean getIsGrounded() {
		return isGrounded;
	}

//	public void setIsGrounded(Boolean isGrounded) {
//		this.isGrounded = isGrounded;
//	}
	
	public Belief computeBeliefProduct( Belief beliefLeft, Belief beliefRight ) {
		Belief beliefProduct = new Belief( );
		// Should constraint to grounded beliefs. Otherwise, I would have to implement the inference procedure 
		// for deriving the sub-beliefs.
		ProbabilityDensityFunction densityProduct = beliefLeft.density.computeProductWith( beliefRight.density );
		beliefProduct.setDistribution( densityProduct );
		
		beliefProduct.setDomain(domain);
		beliefProduct.setSubBelief(subBelief);
		beliefProduct.isGrounded = true;
		
		return beliefProduct;
	}

}
