package inferenceFromUserActions.utilityFunctions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.primitiveData.Assignment;
import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Parameter;
import utilities.ArrayMethods;

/**
 * Extends UtilityFunction to implement a sum of Conditional Gaussians.
 * 
 */

/**
 * @author Cortu01
 *
 */
public class SumOfConditionalGaussians extends UtilityFunction {
	ArrayList< ArrayList< GaussianDensity > > belief;

	/**
	 * Empty Constructor:
	 */
	public SumOfConditionalGaussians() {
		super();
		this.belief = new ArrayList< ArrayList< GaussianDensity > >(); // First dimension refers to the respective Parameter in 'domain'.
		// Second dimension refers to a specific discrete Assignment to that Parameter.
	}
	
	/**
	 * Uninformative Constructor (with Domain specification):
	 */
	public SumOfConditionalGaussians( int n_parameters, int[] cardinalityPerParameter, Parameter utility, Domain independentVariables ) {
		super();
		
		double expectedUtility = 0.5; // See constructor below for using a different prior expected utility. 
		
		ArrayList< ArrayList<GaussianDensity> > belief = new ArrayList< ArrayList<GaussianDensity> >();
		for ( int par = 0; par < n_parameters; par++ ) {
			ArrayList<GaussianDensity> tempArrayList = new ArrayList<GaussianDensity>();
			for ( int ass = 0; ass < cardinalityPerParameter[par]; ass++ ) {
				tempArrayList.add( new GaussianDensity( expectedUtility * 1/n_parameters, 0.25, false ) );
			}
			belief.add( tempArrayList );
		}
		this.belief = belief; // First dimension refers to the respective Parameter in 'domain'. 
		// Second dimension refers to a specific discrete Assignment to that Parameter.
		this.utility = utility;
		this.independentVariables = independentVariables;
	}
	
	/**
	 * Uninformative Constructor (with Domain and Expected Utility specification):
	 */
	public SumOfConditionalGaussians( int n_parameters, int[] cardinalityPerParameter, Parameter utility, Domain independentVariables, double expectedUtility ) {
		super();
		
		ArrayList< ArrayList<GaussianDensity> > belief = new ArrayList< ArrayList<GaussianDensity> >();
		for ( int par = 0; par < n_parameters; par++ ) {
			ArrayList<GaussianDensity> tempArrayList = new ArrayList<GaussianDensity>();
			for ( int ass = 0; ass < cardinalityPerParameter[par]; ass++ ) {
				tempArrayList.add( new GaussianDensity( expectedUtility * 1/n_parameters, expectedUtility/2, false ) );
			}
			belief.add( tempArrayList );
		}
		this.belief = belief; // First dimension refers to the respective Parameter in 'domain'. 
		// Second dimension refers to a specific discrete Assignment to that Parameter.
		this.utility = utility;
		this.independentVariables = independentVariables;
	}
	
	/**
	 * Informative Constructor without known variances (with Domain specification):
	 */
	public SumOfConditionalGaussians( int n_parameters, int[] cardinalityPerParameter, Parameter utility, Domain independentVariables, String fileWithMEANs, int userIndex ) {
		super();
		
		// The whole procedure assumes the same cardinality for all parameters.
		double[][] meanMatrix =  readCSV_double( fileWithMEANs, userIndex, n_parameters, cardinalityPerParameter[0] );
		
		double mean;
		
		ArrayList< ArrayList<GaussianDensity> > belief = new ArrayList< ArrayList<GaussianDensity> >();
		for ( int par = 0; par < n_parameters; par++ ) {
			ArrayList<GaussianDensity> tempArrayList = new ArrayList<GaussianDensity>();
			for ( int ass = 0; ass < cardinalityPerParameter[par]; ass++ ) {
				mean = meanMatrix[par][ass];
				tempArrayList.add( new GaussianDensity( mean, mean/2, false ) );
			}
			belief.add( tempArrayList );
		}
		this.belief = belief; // First dimension refers to the respective Parameter in 'domain'. 
		// Second dimension refers to a specific discrete Assignment to that Parameter.
		this.utility = utility;
		this.independentVariables = independentVariables;
	}
	
	/**
	 * Informative Constructor with known variances (with Domain specification):
	 */
	public SumOfConditionalGaussians( int n_parameters, int[] cardinalityPerParameter, Parameter utility, Domain independentVariables, String fileWithMEANs, String fileWithVARs, int userIndex ) {
		super();
		
		// The whole procedure assumes the same cardinality for all parameters.
		double[][] meanMatrix =  readCSV_double( fileWithMEANs, userIndex, n_parameters, cardinalityPerParameter[0] );
		double[][] varMatrix =  readCSV_double( fileWithVARs, userIndex, n_parameters, cardinalityPerParameter[0] );
		
		double mean;
		double var;
		
		ArrayList< ArrayList<GaussianDensity> > belief = new ArrayList< ArrayList<GaussianDensity> >();
		for ( int par = 0; par < n_parameters; par++ ) {
			ArrayList<GaussianDensity> tempArrayList = new ArrayList<GaussianDensity>();
			for ( int ass = 0; ass < cardinalityPerParameter[par]; ass++ ) {
				mean = meanMatrix[par][ass];
				var = varMatrix[par][ass];
				tempArrayList.add( new GaussianDensity( mean, var, false ) );
			}
			belief.add( tempArrayList );
		}
		this.belief = belief; // First dimension refers to the respective Parameter in 'domain'. 
		// Second dimension refers to a specific discrete Assignment to that Parameter.
		this.utility = utility;
		this.independentVariables = independentVariables;
	}
		
	/**
	 *  Full constructor:
	 */
	public SumOfConditionalGaussians( Parameter utility, Domain independentVariables, 
			ArrayList< ArrayList< GaussianDensity > > belief ) {
		this.utility = utility;
		this.independentVariables = independentVariables;
		this.belief = belief;
	}
	
	@Override
	public ArrayList< ArrayList< GaussianDensity > > getBelief() {
		return belief;
	}
	
	public ProbabilityDensityFunction getBelief( int parameterIndex, int assignmentIndex ) {
		try {
			return belief.get(parameterIndex).get(assignmentIndex);
		} catch (IndexOutOfBoundsException e) {
			String errorText = ">>> Error while trying to access the density for parameter index " + parameterIndex 
					+ " at assignment index " + assignmentIndex + ". ";
			if ( parameterIndex < belief.size() ) {
				errorText = errorText + "There are only " + belief.get(parameterIndex).size() + " assignments for this parameter.";
			} else {
				errorText = errorText + "There are only " + belief.size() + " parameters defined in the density function.";
			}
			throw new IllegalArgumentException(errorText);
		}
	}

	@Override
	public double getExpectedUtility( Assignment assignment ) {
		double expectedUtility = 0;
		int assignmentIndex;
		int parameterIndex; // = 0;
		for ( Parameter parameter : this.independentVariables.getParameters() ) {
			//parameterIndex ++;
			//assignmentIndex 
			parameterIndex = assignment.getDomain().getParameters().indexOf( parameter );
			assignmentIndex = (int) assignment.getValue(parameterIndex);
			//System.out.println("parameterIndex = " + parameterIndex);
			//System.out.println("assignmentIndex = " + assignmentIndex);
			expectedUtility = expectedUtility + belief.get(parameterIndex).get(assignmentIndex).getMean()[0];
		}
		return expectedUtility;
	}
	
	public double getExpectedUtility( int parameterIndex, int assignmentIndex ) {
		try {
			return belief.get(parameterIndex).get(assignmentIndex).getMean()[0];
		} catch (IndexOutOfBoundsException e) {
			String errorText = ">>> Error while trying to access the density for parameter index " + parameterIndex
					+ " at assignment index " + assignmentIndex + ". ";
			if ( parameterIndex < belief.size() ) {
				errorText = errorText + "There are only " + belief.get(parameterIndex).size() + " assignments for this parameter.";
			} else {
				errorText = errorText + "There are only " + belief.size() + " parameters defined in the density function.";
			}
			throw new IllegalArgumentException(errorText);
		}
	}


	@Override
	public void setBelief( ProbabilityDensityFunction gaussianDensity, int parameterIndex, int assignmentIndex ) {
		try {
			if ( gaussianDensity instanceof GaussianDensity ) {
				belief.get(parameterIndex).set( assignmentIndex, (GaussianDensity)gaussianDensity );
			}
		} catch ( ClassCastException e ) {
			String errorText = ">>> Provided density function is not a Gaussian density.";
			throw new IllegalArgumentException(errorText);
		}
		
	}

	@Override
	public void printToConsole() {
		System.out.println("-------------------------------------------------------------------------------------");
		System.out.println(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - Printing Belief: ");
		for ( int ind = 0; ind < this.getBelief().size(); ind++ ) {
			System.out.println( "for Parameter with index = " + ind );
			for ( int ass = 0; ass < this.getBelief().get( ind ).size(); ass++ )  {
				System.out.println( "and assignment = " + ass );
				System.out.println( "" + this.getBelief().get( ind ).get(ass).getMean()[0] );
				System.out.println( "" + this.getBelief().get( ind ).get(ass).getVariance()[0] );
			}
		}		
	}
	
	// Read and store a block from a .csv double matrix:
	static public double[][] readCSV_double( String path, int blockIndex, int n_rowsPerBlock, int n_collumns ) {
		
		BufferedReader br = null;
		double[][] doubleMatrix = null;
		try {
			br = new BufferedReader( new FileReader(path) );
			String[] doubleInfo; // = br.readLine().split(",");
			// a = Integer.parseInt(doubletInfo[0]);
		
			doubleMatrix = new double[ n_rowsPerBlock ][ n_collumns ];
			
			for(int row = 0; row < blockIndex * n_rowsPerBlock; row++) {
				doubleInfo = br.readLine().split(",");
			}
			
			for(int row = 0; row < n_rowsPerBlock; row++) {
				doubleInfo = br.readLine().split(",");
				for(int col = 0 ; col < n_collumns; col++) {
					doubleMatrix[row][col] = Double.parseDouble( doubleInfo[col] );
				}
			}
			
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
		}

		return doubleMatrix;
	}
	
	public void normalise( double maxSum ) {
		
		double[] maxPerVariable = new double[ this.independentVariables.getN_parameters() ];
		
		for( int var = 0; var < this.independentVariables.getN_parameters(); var++ ) {
			maxPerVariable[var] = argmax( var );
		}
		
		double normalisationConstant = ArrayMethods.sumElements( maxPerVariable ) / maxSum;
		
		for( int var = 0; var < this.independentVariables.getN_parameters(); var++ ) {
			for( int asn = 0; asn < this.getBelief().get(var).size(); asn++ ) {
				for( int dim = 0; dim < this.belief.get(var).get(asn).getMean().length; dim++ ) {
					this.belief.get(var).get(asn).setMean( this.belief.get(var).get(asn).getMean()[dim] / normalisationConstant, dim );
				}
			}
		}
		
	}

	private double argmax(int var) {
		
		double max = -10000;
		
		for( int asn = 0; asn < this.getBelief().get(var).size(); asn++ ) {
			if( this.getExpectedUtility(var, asn) > max ) {
				max = this.getExpectedUtility(var, asn);
			}		
		}
		return max;
	}

}
