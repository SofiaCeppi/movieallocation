package inferenceFromUserActions.utilityFunctions;
/**
 * Abstract class for utility functions. Stores the utility and independent variable parameters, 
 * along with our current belief over the relation of the two. Defines abstract methods for 
 * setting and retrieving 'full' and partial utility densities and expected utilities.
 * 
 * This is essentially a wrapper for a belief (which is no longer a Belief object but an arbitrary representation), 
 * with the additional specification of a dependent variable termed 'utility'.
 * 
 */

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.primitiveData.Assignment;
import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Parameter;

/**
 * @author Cortu01
 *
 */
public abstract class UtilityFunction {
	protected Parameter utility; // Dependent variable.
	protected Domain independentVariables;
	protected Object belief;
	
	/**
	 * Empty constructor:
	 */
	public UtilityFunction() {
		this.utility = new Parameter();
		this.independentVariables = new Domain();
		//this.belief = new Belief();
	}
	
	/**
	 *  Full constructor:
	 */
	public UtilityFunction( Parameter utility, Domain independentVariables, Object belief ) {
		this.utility = utility;
		this.independentVariables = independentVariables;
		this.belief = belief; // now is List< List<ProbabilityDistributionFunction> > in SumOfConditionalGaussians
	}

	/**
	 * @return the utility
	 */
	public Parameter getUtility() {
		return utility;
	}

	/**
	 * @param utility the utility to set
	 */
	public void setUtility(Parameter utility) {
		this.utility = utility;
	}

	/**
	 * @return the independentVariables
	 */
	public Domain getIndependentVariables() {
		return independentVariables;
	}

	/**
	 * @param independentVariables the independentVariables to set
	 */
	public void setIndependentVariables(Domain independentVariables) {
		this.independentVariables = independentVariables;
	}

	/**
	 * @return the belief
	 */
	public Object getBelief() {
		return belief;
	}

	/**
	 * @param belief the belief to set
	 */
	public abstract void setBelief( ProbabilityDensityFunction density, int parameterIndex, int assignmentIndex );
	
	public abstract double getExpectedUtility( Assignment assignment );
	
	public abstract void printToConsole();

	public UtilityFunction getUtilityFunctionTrimmedToDomain(Domain subDomain) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
