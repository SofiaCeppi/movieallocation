package inferenceFromUserActions.interactions;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.primitiveData.Assignment;

/**
 * Stores a user's action of selecting an item from a list of items.
 * 
 */

/**
 * @author Cortu01
 *
 */

public class Selection extends Interaction {
	protected int action;

	public Selection( List<Assignment> items, int action ) {
		this.itemsList = items;
		this.setAction(action);
	}

	/**
	 * @return the action
	 */
	public int getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(int action) {
		this.action = action;
	}

	/**
	 * @return an ArrayList of Selection objects, one for each item not selected, 
	 * with the item list containing that item and the selected item (and 
	 * indicating this selection). This represents a list of pairwise comparisons 
	 * between the selected object and every other object in the list. 
	 * 
	 */
	public ArrayList<Selection> getPairwiseComparisons() {
		ArrayList<Selection> pairwiseComparisons = new ArrayList<Selection>();
		for ( int itm = 0; itm < this.getItemListSize(); itm++ ) {
			if ( itm != this.action ) {
				ArrayList<Assignment> list = new ArrayList<Assignment>();
				list.add( itemsList.get(this.action) );
				list.add( itemsList.get(itm) );
				Selection pairwiseComparison = new Selection(list, 0);
				pairwiseComparisons.add(pairwiseComparison);
			}
		}
		return pairwiseComparisons;
	}
}
