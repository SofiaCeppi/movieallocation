package inferenceFromUserActions.interactions;
import java.util.List;

import inferenceFromUserActions.primitiveData.Assignment;

/**
 * Abstract class for storing a user's interaction with a list of one or more items.
 * 
 */

/**
 * @author Cortu01
 *
 */
public abstract class Interaction {
	protected List<Assignment> itemsList;
	protected Object action;
	
	/**
	 * @return the itemsList
	 */
	public List<Assignment> getItemsList() {
		return itemsList;
	}
	
	/**
	 * @param itemsList the itemsList to set
	 */
	public void setItemsList(List<Assignment> itemsList) {
		this.itemsList = itemsList;
	}
	
	public int getItemListSize() {
		return itemsList.size();
	}
	

}
