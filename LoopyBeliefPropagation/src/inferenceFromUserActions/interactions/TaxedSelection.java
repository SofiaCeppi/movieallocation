/**
 * 
 */
package inferenceFromUserActions.interactions;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.primitiveData.Assignment;

/**
 * @author Cortu01
 *
 */
public class TaxedSelection extends Selection {
	/* The values in "tax" are pure utility values which are added to the utility of the 
	 * respective item. So, even though I've named it a tax, it would have to be negated 
	 * to function as one. */

	private List<Double> tax;
	/**
	 * @param items
	 * @param action
	 */
	public TaxedSelection(List<Assignment> items, List<Double> tax, int action) {
		super(items, action);
		this.setTax(tax);
	}
	
	public List<Double> getTax() {
		return tax;
	}
	
	public void setTax(List<Double> tax) {
		this.tax = tax;
	}
	
	/**
	 * @return an ArrayList of TaxedSelection objects, one for each item not 
	 * selected, with the item list containing that item and the selected item 
	 * (and indicating this selection). This represents a list of pairwise 
	 * comparisons between the selected object and every other object in the 
	 * list. 
	 * 
	 * This overrides the near-identical function in Selection. The main 
	 * difference lies in the output of TaxedSelection objects that state the 
	 * respective tax pairs.
	 * 
	 */
	public ArrayList<Selection> getPairwiseComparisons() {
		ArrayList<Selection> pairwiseComparisons = new ArrayList<Selection>();
				
		for ( int itm = 0; itm < this.getItemListSize(); itm++ ) {
			
			if ( itm != this.action ) {
				ArrayList<Assignment> list = new ArrayList<Assignment>();
				list.add( itemsList.get(this.action) );
				list.add( itemsList.get(itm) );
				
				ArrayList<Double> tax = new ArrayList<Double>();
				tax.add( this.tax.get(this.action) );
				tax.add( this.tax.get(itm) );
				
				TaxedSelection pairwiseComparison = new TaxedSelection(list, tax, 0);
				pairwiseComparisons.add(pairwiseComparison);
			}
		}
		return pairwiseComparisons;
	}

}
