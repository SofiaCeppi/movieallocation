/**
 * 
 */
package inferenceFromUserActions.interactions;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.primitiveData.Assignment;

/**
 * Stores a user's action of rating an item (could be extended easily to > 1 items).
 * 
 */

/**
 * @author Cortu01
 *
 */
public class Rating extends Interaction {
	private double action;
	private static double noise = 0.05;

	/**
	 * Full constructor I (list of 1 item):
	 */
	public Rating( List<Assignment> items, double action ) {
		this.itemsList = items;
		this.setAction(action);
	}
	
	/**
	 * Full constructor II (1 item):
	 */
	public Rating( Assignment item, double action ) {
		this.itemsList = new ArrayList<Assignment>();
		this.itemsList.add(item);
		this.setAction(action);
	}
	
	/**
	 * @return the action
	 */
	public double getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(double action) {
		this.action = action;
	}

	public Assignment getRatedItem() {
		return itemsList.get(0);
	}
	
	public static double getNoise() {
		return Rating.noise;
	}

	public void setNoise( double noise ) {
		Rating.noise = noise;
	}
	
}
