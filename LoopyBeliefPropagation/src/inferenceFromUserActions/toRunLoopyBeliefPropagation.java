package inferenceFromUserActions;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.interactions.Rating;
import inferenceFromUserActions.loopyBeliefPropagation.LoopyBeliefPropagation;
import inferenceFromUserActions.loopyBeliefPropagation.PrepareForLBP_fromSumOf1D_CG_rating;
import inferenceFromUserActions.primitiveData.DiscreteAssignment;
import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Parameter;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;

/**
 * Example code for running a loopy belief propagation algorithm in order to perform a step of utility 
 * function belief update, following a user's action of rating an item.
 * 
 */

/**
 * @author Cortu01
 *
 */

public class toRunLoopyBeliefPropagation {

	public toRunLoopyBeliefPropagation() {
		
		String[] string = { "Test", "Test" };
		
		main(string);
	}

	public static void main( String[] args ) {
		// An example use of LoopyBeliefPropagation for updating our belief
		// over a user's utility function after they have rated an item.
		int n_parameters = 4;
		int[] cardinalityPerParameter = { 5, 4, 3, 4 };
		
		Parameter utilityParameter = new Parameter( "Utility", "double", false, "Real" );
		
		// The definitions below imply much more information then what we are actually working with.
		// It is up to possible future extensions to make use of such descriptions. For now, we are 
		// simply working with discrete assignment domains from 0 to some other integer.
		List<Parameter> parameters = new ArrayList<Parameter>();
		int[] assignmentSpace1 = {0, 4};
		Parameter parameter1 = new Parameter( "Parameter1", "int", true, assignmentSpace1 );
		String[] assignmentSpace2 = {"Blue", "Red", "Green", "Grey"}; 
		Parameter parameter2 = new Parameter( "Parameter2", "String", false, assignmentSpace2 );
		String assignmentSpace3 = "PositiveInteger"; 
		Parameter parameter3 = new Parameter( "Price", "int", false, assignmentSpace3 );
		String assignmentSpace4 = "Integer"; 
		Parameter parameter4 = new Parameter( "Size", "int", false, assignmentSpace4 );
		parameters.add(parameter1);
		parameters.add(parameter2);
		parameters.add(parameter3);
		parameters.add(parameter4);
		
		Domain domain = new Domain( parameters );
		
		SumOfConditionalGaussians utilityFunction = new SumOfConditionalGaussians( n_parameters, cardinalityPerParameter, utilityParameter, domain );
		
		ArrayList<Object> values = new ArrayList<Object>();
		values.add(1);
		values.add(0);
		values.add(2);
		values.add(3);
		
		double assignedRating = 1.0;
		
		SumOfConditionalGaussians updatedUtilityFunction = utilityFunction;
		for ( int iteration = 0; iteration < 1; iteration++ ) {

			Rating interaction = new Rating(new DiscreteAssignment(domain, values), assignedRating);

			PrepareForLBP_fromSumOf1D_CG_rating problemSpecification = new PrepareForLBP_fromSumOf1D_CG_rating(
					updatedUtilityFunction, interaction);//utilityFunction

			LoopyBeliefPropagation problemSolver = new LoopyBeliefPropagation(problemSpecification.getFactorGraph(),
					problemSpecification.getVariablePriors());
			problemSolver.performLoopyBeliefPropagation();

			List<ProbabilityDensityFunction> potentials = problemSolver.getPotentials();
			problemSpecification.savePosterior(potentials);

			updatedUtilityFunction = (SumOfConditionalGaussians) problemSpecification
					.getFullPosterior();
			updatedUtilityFunction.printToConsole();
			
			assignedRating = 0.0;
		}

	}

}
