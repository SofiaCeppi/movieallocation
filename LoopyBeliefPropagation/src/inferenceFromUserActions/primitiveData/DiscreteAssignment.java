/**
 * 
 */
package inferenceFromUserActions.primitiveData;

import java.util.List;

/**
 * @author Cortu01
 *
 */
public class DiscreteAssignment extends Assignment {
	//private List<Integer> values;

	/**
	 * Empty constructor:
	 */
	public DiscreteAssignment() {}

	/**
	 * @param domain
	 * @param values
	 */
	public DiscreteAssignment(Domain domain, List<Object> values) {
		super(domain, values);

		try {
			if ( !values.isEmpty() && !(values.get(0) instanceof Integer) ) {
				String errorText = ">>> Error while creating new DiscreteAssignment. "
						+ "The values for the assignments do not appear to be Integers.";
				throw new IllegalArgumentException(errorText);
			}
			
		} catch(IllegalArgumentException e) {
			throw new RuntimeException(e);
		}

	}

//	public void setValues( List<Integer> values ) {
//		this.values = values;		
//	}
	
//	/**
//	 * @return the values
//	 */
//	public List<Object> getValues() {
//		return values;
//	}
	
}
