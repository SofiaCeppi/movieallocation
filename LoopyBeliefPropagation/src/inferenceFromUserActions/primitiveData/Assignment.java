package inferenceFromUserActions.primitiveData;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines a specific assignment of values to a list of parameters, as defined by their joint domain.
 * 
 */

/**
 * @author Cortu01
 *
 */
public abstract class Assignment {
	private Domain domain;
	protected List<Object> values;

	/**
	 * Empty constructor:
	 */	
	public Assignment(){
		this.domain = new Domain();
		this.values = new ArrayList<Object>();
	}
	
	/**
	 * Full constructor:
	 */
	public Assignment( Domain domain, List<Object> values ){
		this.domain = domain;
		this.values = values; 
	}

	/**
	 * @return the domain
	 */
	public Domain getDomain() {
		return domain;
	}

	/**
	 * @param domain the domain to set
	 */
	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	/**
	 * @return the values
	 */
	public List<Object> getValues() {
		return values;
	}

	/**
	 * @return the values
	 */
	public Object getValue( Parameter parameter ) {
		return values.get( domain.getParameters().indexOf( parameter ) );
	}
	
	/**
	 * @return the values
	 */
	public Object getValue( int parameterIndex ) {
		return values.get(parameterIndex);
	}
	
	/**
	 * @param values the values to set
	 */
	public void setValues(List<Object> values) {
		this.values = values;
	}
	
}
