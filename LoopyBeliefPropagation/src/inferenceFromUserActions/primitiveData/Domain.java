package inferenceFromUserActions.primitiveData;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for "List<Parameter>".
 * 
 */


/**
 * @author Cortu01
 *
 */
public class Domain {
	private List<Parameter> parameters;
	
	/**
	 * Empty constructor:
	 */	
	public Domain(){
		this.setParameters(new ArrayList<Parameter>());
	}
	
	/**
	 * Full constructor:
	 */
	public Domain( List<Parameter> parameters ){
		this.parameters = new ArrayList<Parameter>();
		for ( Parameter par : parameters ) {
			this.parameters.add(par);
		}
		
	}

	/**
	 * Single parameter constructor:
	 */
	public Domain(Parameter parameter) {
		List<Parameter> parameters = new ArrayList<Parameter>();
		parameters.add(parameter);
		this.parameters = parameters;
	}

	/**
	 * @return the number of parameters
	 */
	public int getN_parameters() {
		return parameters.size();
	}
	
	/**
	 * @return the parameters
	 */
	public List<Parameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public Domain getSubDomainFromTrimmingParametersWithMatchingAssignments(Assignment assignment, Assignment assignment2) {
		Domain subDomain = new Domain(this.getParameters());
		
		try {
			if( assignment.getDomain() != assignment2.getDomain() ) {
				String errorText = ">>> Error while intersecting the two Assignments. They refer to different Domains.";
				throw new IllegalArgumentException(errorText);
			} else if( assignment.getDomain() != this ) {
				String errorText = ">>> Error while intersecting the two Assignments. They are not assignments to this Domain "
						+ "(but both refer to the same other Domain).";
				throw new IllegalArgumentException(errorText);
			}
		} catch( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		
		// TODO : This code is buggy, throwing "Exception in thread "main" java.util.ConcurrentModificationException".
		List<Parameter> temp = subDomain.getParameters();
		for( Parameter par : subDomain.parameters ) {
			if ( assignment.getValue(par) == assignment2.getValue(par) ) {
				temp.remove(par);
			}
		}
		subDomain.setParameters(temp);
		
		return subDomain;
		
	}
	
}
