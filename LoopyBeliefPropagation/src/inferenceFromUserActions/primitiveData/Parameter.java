package inferenceFromUserActions.primitiveData;
/**
 * Defines a parameter's name, and solution/assignment space.
 * 
 */

/**
 * @author Cortu01
 *
 */
public class Parameter {
	private String name;
	private String assignmentType;
	private boolean isAssignmentSpaceRange;
	private Object assignmentSpace;

	/**
	 * Empty constructor:
	 */
	public Parameter() {}
	
	/**
	 * Full constructor:
	 */
	public Parameter( String name, String assignmentType, boolean isAssignmentSpaceRange, Object assignmentSpace ) {
		this.name = name;
		this.assignmentType = assignmentType;
		this.isAssignmentSpaceRange = isAssignmentSpaceRange;
		this.assignmentSpace = assignmentSpace;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the assignmentType
	 */
	public String getAssignmentType() {
		return assignmentType;
	}

	/**
	 * @param assignmentType the assignmentType to set
	 */
	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}

	/**
	 * @return the assignmentSpace
	 */
	public Object getAssignmentSpace() {
		return assignmentSpace;
	}

	/**
	 * @param assignmentSpace the assignmentSpace to set
	 */
	public void setAssignmentSpace(Object assignmentSpace) {
		this.assignmentSpace = assignmentSpace;
	}

	/**
	 * @return the isAssignmentSpaceRange
	 */
	public boolean isAssignmentSpaceRange() {
		return isAssignmentSpaceRange;
	}

	/**
	 * @param isAssignmentSpaceRange the isAssignmentSpaceRange to set
	 */
	public void setAssignmentSpaceRange(boolean isAssignmentSpaceRange) {
		this.isAssignmentSpaceRange = isAssignmentSpaceRange;
	}

}
