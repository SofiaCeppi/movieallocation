/**
 * 
 */
package inferenceFromUserActions.primitiveData;

import java.util.ArrayList;

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;

/**
 * @author Cortu01
 *
 */
public class Factor_pdf extends Factor {
	// Wrapper for a Probability Density Function.
	private ProbabilityDensityFunction probabilityDensityFunction;

	/**
	 * Empty constructor:
	 */
	public Factor_pdf() {}

	/**
	 * Constructor for wrapping a ProbabilityDensityFunction:
	 */
	public Factor_pdf( int factorIndex, ProbabilityDensityFunction probabilityDensityFunction ) {
		super( factorIndex );
		this.probabilityDensityFunction = probabilityDensityFunction;		
	}

	/**
	 * @return the probabilityDensityFunction
	 */
	public ProbabilityDensityFunction getProbabilityDensityFunction() {
		return probabilityDensityFunction;
	}

	/**
	 * @param probabilityDensityFunction the probabilityDensityFunction to set
	 */
	public void setProbabilityDensityFunction(ProbabilityDensityFunction probabilityDensityFunction) {
		this.probabilityDensityFunction = probabilityDensityFunction;
	}

	@Override
	public Message computeMessageOut( int variableOutIndex, ArrayList<Message> messagesIn ) {
		// This is correct only for the simple scenarios we are looking at: (TODO more specifically? - Will it hold for pairwise comparisons?) 
		ProbabilityDensityFunction aggregate_pdf = Message.computeProductOfMessageContent( messagesIn );
		aggregate_pdf = aggregate_pdf.computeProductWith( this.probabilityDensityFunction );
		boolean emptyContent = aggregate_pdf.isOne();
		return new Message( aggregate_pdf, this.factorIndex, variableOutIndex, true, emptyContent );
	}

	@Override
	public Message computeMessageOut( int variableOutIndex ) {
		boolean emptyContent = this.probabilityDensityFunction.isOne();
		System.out.println("( In Factor_pdf, line 57 : ) emptyContent = " + emptyContent );
		return new Message( this.probabilityDensityFunction, this.factorIndex, variableOutIndex, true, emptyContent );
	}

}
