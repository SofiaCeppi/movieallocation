/**
 * 
 */
package inferenceFromUserActions.primitiveData;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;

/**
 * @author Cortu01
 *
 */
public class Message {
	private ProbabilityDensityFunction content;
	private int factorIndex;
	private int variableIndex;
	private boolean isFactorToVariable;
	private boolean isVariableToFactor;
	private boolean emptyContent;
	
	/**
	 * Empty constructor:
	 */
	public Message() {
		this.setEmptyContent(true);
	}

	/**
	 * Lazy constructor (sets message content to 'empty'):
	 */
	public Message(boolean emptyContent) {
		this.setEmptyContent(emptyContent);
	}
	
	/**
	 * Partial constructor (no content):
	 */
	public Message( int factorIndex, int variableIndex, boolean isFactorToVariable ) {
		this.factorIndex = factorIndex;
		this.variableIndex = variableIndex;
		this.isFactorToVariable = isFactorToVariable;
		this.isVariableToFactor = !isFactorToVariable;
		this.setEmptyContent(true);
	}

	/**
	 * Full constructor:
	 */
	public Message( ProbabilityDensityFunction content, int factorIndex, int variableIndex, boolean isFactorToVariable, boolean emptyContent ) {
		this.content = content.copy();
		this.factorIndex = factorIndex;
		this.variableIndex = variableIndex;
		this.isFactorToVariable = isFactorToVariable;
		this.isVariableToFactor = !isFactorToVariable;
		this.setEmptyContent(emptyContent);
	}
	
	/**
	 * @return the factorIndex
	 */
	public int getFactorIndex() {
		return factorIndex;
	}
	/**
	 * @param factorIndex the factorIndex to set
	 */
	public void setFactorIndex(int factorIndex) {
		this.factorIndex = factorIndex;
	}
	/**
	 * @return the variableIndex
	 */
	public int getVariableIndex() {
		return variableIndex;
	}
	/**
	 * @param variableIndex the variableIndex to set
	 */
	public void setVariableIndex(int variableIndex) {
		this.variableIndex = variableIndex;
	}
	/**
	 * @return the isFactorTovariable
	 */
	public boolean isFactorToVariable() {
		return isFactorToVariable;
	}
	/**
	 * @param isFactorTovariable the isFactorTovariable to set
	 */
	public void setFactorToVariable(boolean isFactorToVariable) {
		this.isFactorToVariable = isFactorToVariable;
		this.isVariableToFactor = !isFactorToVariable;
	}
	/**
	 * @return the isVariabletoFactor
	 */
	public boolean isVariableToFactor() {
		return isVariableToFactor;
	}
	/**
	 * @param isVariabletoFactor the isVariabletoFactor to set
	 */
	public void setVariableToFactor(boolean isVariableToFactor) {
		this.isVariableToFactor = isVariableToFactor;
	}

	/**
	 * @return the content
	 */
	public ProbabilityDensityFunction getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(ProbabilityDensityFunction content) {
		this.content = content;
	}

	/**
	 * @return the emptyContent
	 */
	public boolean isEmptyContent() {
		return emptyContent;
	}

	/**
	 * @param emptyContent the emptyContent to set
	 */
	public void setEmptyContent(boolean emptyContent) {
		this.emptyContent = emptyContent;
	}
	
	/**
	 * @return the index of the Message element in emptyContent the emptyContent to set
	 */
	public static int getIndexOfMatchingMessage( List<Message> messages, int factorIndex, int variableIndex, boolean isFactorToVariable ) {
		boolean messageNotFound = true;
		int messageIndex = -1;
		while ( messageNotFound ) {
			messageIndex++;
			if ( messages.get( messageIndex ).getFactorIndex() == factorIndex 
					&& messages.get( messageIndex ).getVariableIndex() == variableIndex 
					&& messages.get( messageIndex ).isFactorToVariable() == isFactorToVariable ) {
				messageNotFound = false;
			}
		}
		
		if ( messageNotFound ) {
			messageIndex = -1;
		}
		
		return messageIndex;
			
	}

	public static ProbabilityDensityFunction computeProductOfMessageContent( ArrayList<Message> incomingMessages ) {
		ProbabilityDensityFunction pdf = incomingMessages.get(0).getContent();
		for ( int msg = 1; msg < incomingMessages.size(); msg++ ) {
			pdf = pdf.computeProductWith( incomingMessages.get(msg).getContent() );
		}
		return pdf;
	}

	public static ProbabilityDensityFunction sumWeightedMessageContent( ArrayList<Message> incomingMessages, double[] weights ) {	 
		int msg = 0;
		boolean noNonEmptyMessageFound = true;
		while ( noNonEmptyMessageFound && msg < incomingMessages.size() ) {
			if ( !incomingMessages.get(msg).isEmptyContent() ) {
				noNonEmptyMessageFound = false;
			}
			msg++;
		}
		
		ProbabilityDensityFunction pdf;
		if ( !noNonEmptyMessageFound ) {
			msg = msg - 1;
			pdf = incomingMessages.get(msg).getContent().scale( weights[msg] );
			for ( int msg2 = msg + 1; msg2 < incomingMessages.size(); msg2++ ) {
				pdf = pdf.sumWith( incomingMessages.get(msg2).getContent().scale( weights[msg2] ) );
			}
		} else {
			pdf = new GaussianDensity( 0.0, 0.0, true);
		}
				
		return pdf;
	}
}
