/**
 * 
 */
package inferenceFromUserActions.primitiveData;

import java.util.ArrayList;

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;

/**
 * @author Cortu01
 *
 */
public class Factor_sum extends Factor {
	// Has directionality in compute message out.
	private int sumVariableIndex;
	//private double weights[];

	/**
	 * Empty constructor:
	 */
	public Factor_sum() {}
	
	/**
	 * Full constructor (with default weights of 1):
	 */
	public Factor_sum( int factorIndex, int sumVariableIndex ) { // , int n_variables
		super( factorIndex );	
		this.sumVariableIndex = sumVariableIndex;
	}

	@Override
	public Message computeMessageOut( int variableOutIndex, ArrayList<Message> messagesIn ) {
		double weights[] = new double[ messagesIn.size() ];
		if ( this.sumVariableIndex == variableOutIndex ) {
			
			for ( int var = 0; var < messagesIn.size(); var++ ) {
				weights[var] = 1;
			}
			//weights[sumVariableIndex] = 0;
			
		} else {
			
			for ( int var = 0; var < messagesIn.size(); var++ ) {
				if ( messagesIn.get(var).getVariableIndex() != this.sumVariableIndex ) {
					weights[var] = -1;
				} else {
					weights[var] = +1;
				}
				
			}
						
		}
				
		ProbabilityDensityFunction sum_pdf = Message.sumWeightedMessageContent( messagesIn, weights );
		boolean emptyContent = sum_pdf.isOne();
		
		return new Message( sum_pdf, this.factorIndex, variableOutIndex, true, emptyContent );
	}

	@Override
	public Message computeMessageOut( int variableOutIndex ) {
		// TODO Would this ever be the case? If so, it should probably return an empty Message.
		return null;
	}

}
