/**
 * 
 */
package inferenceFromUserActions.primitiveData;

import java.util.ArrayList;

/**
 * @author Cortu01
 *
 */
public abstract class Factor {
//	int incomingVariables[];
//	int outgoingVariables[];
	int factorIndex;

	/**
	 * Empty constructor:
	 */
	public Factor() {}
	
	/**
	 * Full constructor:
	 */
	public Factor( int factorIndex) {
		this.factorIndex = factorIndex;
		}
	
//	/**
//	 * Full constructor:
//	 */
//	public Factor( int incomingVariables[], int outgoingVariables[] ) {
//		this.incomingVariables = incomingVariables;
//		this.outgoingVariables = outgoingVariables;
//	}
	
	/**
	 * @return the outcoming message:
	 * 
	 * Returns the Message from this factor to the indicated variable. Does not check 
	 * if a Message should be sent and assumes that there are no incoming messages to 
	 * the factor from OTHER variables.
	 */
	public abstract Message computeMessageOut( int variableOutIndex );
	
	/**
	 * @return the outcoming message:
	 * 
	 * Returns the Message from this factor to the indicated variable. Does not check 
	 * if a Message should be sent and assumes that all incoming messages to the factor 
	 * from OTHER variables are given by "messagesIn".
	 */
	public abstract Message computeMessageOut( int variableOutIndex, ArrayList<Message> messagesIn );

	/**
	 * @return the factorIndex
	 */
	public int getFactorIndex() {
		return factorIndex;
	}
	
//	/**
//	 * @return the incomingVariables
//	 */
//	public int[] getIncomingVariables() {
//		return incomingVariables;
//	}
//
//	/**
//	 * @param incomingVariables the incomingVariables to set
//	 */
//	public void setIncomingVariables(int[] incomingVariables) {
//		this.incomingVariables = incomingVariables;
//	}
//
//	/**
//	 * @return the outgoingVariables
//	 */
//	public int[] getOutgoingVariables() {
//		return outgoingVariables;
//	}
//
//	/**
//	 * @param outcomingVariables the outcomingVariables to set
//	 */
//	public void setOutgoingVariables(int[] outgoingVariables) {
//		this.outgoingVariables = outgoingVariables;
//	}

}
