/**
 * 
 */
package inferenceFromUserActions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.interactions.Selection;
import inferenceFromUserActions.interactions.TaxedSelection;
import inferenceFromUserActions.loopyBeliefPropagation.PrepareForLBP_fromSumOf1D_CG_pairwiseComparison;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;
import jskills.GameInfo;
import jskills.IPlayer;
import jskills.ITeam;
import jskills.Player;
import jskills.Rating;
import jskills.Team;
import jskills.trueskill.TwoTeamTrueSkillCalculator;

/**
 * This class acts as an intermediate between our specification of a 1 dimensional Sum of Conditional Gaussians belief 
 * and pairwise comparison, and the JSkills tool. The latter is used for updating our belief given the interaction.
 * 
 */

/**
 * @author Cortu01
 *
 */
public class JSkillsIntegrator {
	Map<IPlayer, Rating> newRatings;
	private PrepareForLBP_fromSumOf1D_CG_pairwiseComparison problemSpecification;
	private TwoTeamTrueSkillCalculator calculator;
	private GameInfo gameInfo;
	private Collection<ITeam> teams;
	private Team team1;
	private Team team2;

	public JSkillsIntegrator(PrepareForLBP_fromSumOf1D_CG_pairwiseComparison problemSpecification) {
		this.problemSpecification = problemSpecification;
		this.calculator = new TwoTeamTrueSkillCalculator();		
		this.gameInfo = GameInfo.getDefaultGameInfo();
		
		Player<Integer> player;
		// Loop a new "player" for each parameter and access the respective prior for item_0:
		this.team1 = new Team();
		for ( int par = 0; par < ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() ).getIndependentVariables(
				).getN_parameters(); par++ ) {
			player = new Player<Integer>(par);
			double mean = ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() 
					).getExpectedUtility(par, (int) problemSpecification.getInteraction(
					).getItemsList().get(0).getValue(par) );
			double standardDeviation = ( (GaussianDensity) ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() 
					).getBelief(par, (int) problemSpecification.getInteraction(
					).getItemsList().get(0).getValue(par) ) ).getStandardDeviation()[0];
			team1.addPlayer( player, new Rating( mean, standardDeviation ) );
		}
		
		if ( problemSpecification.getInteraction() instanceof TaxedSelection ) {
			player = new Player<Integer>( ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() ).getIndependentVariables(
					).getN_parameters() );
			double mean = ( (TaxedSelection) problemSpecification.getInteraction() ).getTax().get(0);
			double standardDeviation = 0.1; //small standard deviation, emulating a constant
			team1.addPlayer( player, new Rating( mean, standardDeviation ) );
		}

		// Loop a new "player" for each parameter and access the respective prior for item_1:
		this.team2 = new Team();
		for ( int par = 0; par < ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() ).getIndependentVariables().getN_parameters(); par++ ) {
			int nudge = 0;
			if ( problemSpecification.getInteraction() instanceof TaxedSelection ) {
				nudge = 1;
			}
			player = new Player<Integer>( nudge + par + ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() ).getIndependentVariables().getN_parameters() );
			double mean = ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() 
					).getExpectedUtility(par, (int) problemSpecification.getInteraction(
					).getItemsList().get(1).getValue(par) );
			double standardDeviation = ( (GaussianDensity) ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() 
					).getBelief(par, (int) problemSpecification.getInteraction(
					).getItemsList().get(1).getValue(par) ) ).getStandardDeviation()[0];
			team2.addPlayer( player, new Rating( mean, standardDeviation ) );
		}
		
		if ( problemSpecification.getInteraction() instanceof TaxedSelection ) {
			player = new Player<Integer>( ( (SumOfConditionalGaussians) problemSpecification.getFullPrior() ).getIndependentVariables(
					).getN_parameters() * 2 + 1 );
			double mean = ( (TaxedSelection) problemSpecification.getInteraction() ).getTax().get(1);
			double standardDeviation = 0.1; //small standard deviation, emulating a constant
			team2.addPlayer( player, new Rating( mean, standardDeviation ) );
		}

        this.teams = Team.concat(team1, team2);		
	}

	public void run() {		
        if ( ( (Selection) problemSpecification.getInteraction() ).getAction() == 0  ) {
        	this.newRatings = this.calculator.calculateNewRatings(this.gameInfo, this.teams, 1, 2);
        } else {
        	this.newRatings = this.calculator.calculateNewRatings(this.gameInfo, this.teams, 2, 1);
        }        
	}
	
	public List<ProbabilityDensityFunction> getPotentials() {
		List<ProbabilityDensityFunction> potentials = new ArrayList<ProbabilityDensityFunction>();
		
		Rating value;
		for ( Map.Entry<IPlayer, Rating> playerEntry : this.newRatings.entrySet() ) {
		value = playerEntry.getValue();
		potentials.add( new GaussianDensity( value.getMean(), value.getVariance(), false) );
		}
		if ( this.problemSpecification.getInteraction() instanceof TaxedSelection ) {
			// Remove the potentials that refer to the taxation "players" (last parameter for each item):
			int lengthOfPotentials = potentials.size();
			potentials.remove( lengthOfPotentials   - 1 );
			potentials.remove( lengthOfPotentials/2 - 1 );
		}
		return potentials;
	}
	
	/**
	 * @return the newRatings
	 */
	public Map<IPlayer, Rating> getNewRatings() {
		return newRatings;
	}

	/**
	 * @param newRatings the newRatings to set
	 */
	public void setNewRatings(Map<IPlayer, Rating> newRatings) {
		this.newRatings = newRatings;
	}

	/**
	 * @return the problemSpecification
	 */
	public PrepareForLBP_fromSumOf1D_CG_pairwiseComparison getProblemSpecification() {
		return problemSpecification;
	}

	/**
	 * @param problemSpecification the problemSpecification to set
	 */
	public void setProblemSpecification(PrepareForLBP_fromSumOf1D_CG_pairwiseComparison problemSpecification) {
		this.problemSpecification = problemSpecification;
	}

	/**
	 * @return the calculator
	 */
	public TwoTeamTrueSkillCalculator getCalculator() {
		return calculator;
	}

	/**
	 * @param calculator the calculator to set
	 */
	public void setCalculator(TwoTeamTrueSkillCalculator calculator) {
		this.calculator = calculator;
	}

	/**
	 * @return the gameInfo
	 */
	public GameInfo getGameInfo() {
		return gameInfo;
	}

	/**
	 * @param gameInfo the gameInfo to set
	 */
	public void setGameInfo(GameInfo gameInfo) {
		this.gameInfo = gameInfo;
	}

	/**
	 * @return the teams
	 */
	public Collection<ITeam> getTeams() {
		return teams;
	}

	/**
	 * @param teams the teams to set
	 */
	public void setTeams(Collection<ITeam> teams) {
		this.teams = teams;
	}

	/**
	 * @return the team1
	 */
	public Team getTeam1() {
		return team1;
	}

	/**
	 * @param team1 the team1 to set
	 */
	public void setTeam1(Team team1) {
		this.team1 = team1;
	}

	/**
	 * @return the team2
	 */
	public Team getTeam2() {
		return team2;
	}

	/**
	 * @param team2 the team2 to set
	 */
	public void setTeam2(Team team2) {
		this.team2 = team2;
	}

}
