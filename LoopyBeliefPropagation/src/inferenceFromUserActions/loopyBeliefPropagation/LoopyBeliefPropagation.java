/**
 * 
 */
package inferenceFromUserActions.loopyBeliefPropagation;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.factorGraphGenerators.FactorGraph;
import inferenceFromUserActions.primitiveData.Factor;
import inferenceFromUserActions.primitiveData.Message;
import utilities.ArrayMethods;

/**
 * @author Cortu01
 *
 */
public class LoopyBeliefPropagation {
	private FactorGraph factorGraph;
	private int n_messages;
	private List<Message> messages;
	private int[] messageComputationOrder;
	private List<ProbabilityDensityFunction> prior;
	private List< List<ProbabilityDensityFunction> > priorHistory; // Stores the results after each iteration of LoopyBeliefPropagation.
	private List<ProbabilityDensityFunction> potentials; // The posterior as it is being updated during the loops.
	private int currentIteration = 0;
	private boolean detailedOutput = true;

	
	// Prior/posterior must be of length = factorGraph.getN_variables
	

	/**
	 * Full constructor:
	 */
	public LoopyBeliefPropagation( FactorGraph factorGraph, List<ProbabilityDensityFunction> prior, int[] messageComputationOrder ) { 
		 this( factorGraph, prior );
		 this.messageComputationOrder = messageComputationOrder;		 
	}
	
	/**
	 * Full constructor sans messageComputationOrder:
	 */
	public LoopyBeliefPropagation( FactorGraph factorGraph, List<ProbabilityDensityFunction> prior ) { 
		try {
			
			if ( factorGraph.getN_variables() != prior.size() ) {
								
				String errorText = ">>> Error while creating new LoopyBeliefPropagation procedure. The prior "
						+ " has a size that is not consistent with the number of variables over which the "
						+ "factorGraph is constructed.";
				throw new IllegalArgumentException(errorText);
			}
			this.factorGraph = factorGraph;
			
			this.prior = prior;
			this.priorHistory = new ArrayList< List<ProbabilityDensityFunction> >();
			this.priorHistory.add(prior);

			this.n_messages = 2 * ArrayMethods.sumElements(factorGraph.getFactorGraphEdges());
			this.messages = new ArrayList<Message>();
			
			// Adding empty Messages from Factors to variables:
			for ( int factorIndex = 0; factorIndex < factorGraph.getN_factors(); factorIndex++ ) {
				for ( int variableIndex = 0; variableIndex < factorGraph.getN_variables(); variableIndex++ ) { 
					if ( factorGraph.checkEdge( factorIndex, variableIndex ) ) {
						this.messages.add( new Message( new GaussianDensity(0.0, 0.0, true), factorIndex, variableIndex, true, true ) );
					}
				}
			}
			
			// Adding empty Messages from variables to Factors:
			for ( int factorIndex = 0; factorIndex < factorGraph.getN_factors(); factorIndex++ ) {
				for ( int variableIndex = 0; variableIndex < factorGraph.getN_variables(); variableIndex++ ) { 
					if ( factorGraph.checkEdge( factorIndex, variableIndex ) ) {
						this.messages.add( new Message( new GaussianDensity(0.0, 0.0, true), factorIndex, variableIndex, false, true ) );
					}
				}
			}

			this.potentials = prior; // Initially potentials are "empty":
			for ( ProbabilityDensityFunction pdf : this.potentials ) {
				pdf.setOne(true);
			}
			
		} catch (IllegalArgumentException e){
			throw new RuntimeException(e);
		}
		
		// Set computation order as "Factors to Variables", then "Variables to Factors":
		this.messageComputationOrder = new int[this.n_messages];
		for ( int msg = 0; msg < n_messages; msg++ ) {
			this.messageComputationOrder[msg] = msg;
		}
		
	}


	/**
	 * @return the potentials
	 */
	public List<ProbabilityDensityFunction> getPotentials() {
		return potentials;
	}

	/**
	 * @param potentials the potentials to set
	 */
	public void setPotentials(List<ProbabilityDensityFunction> potentials) {
		this.potentials = potentials;
	}
	
	/**
	 * @return the priorHistory
	 */
	public List<List<ProbabilityDensityFunction>> getPriorHistory() {
		return priorHistory;
	}

	/**
	 * @return the prior
	 */
	public List<ProbabilityDensityFunction> getPrior() {
		return prior;
	}
	
	/**
	 * @return the detailedOutput
	 */
	public boolean isDetailedOutput() {
		return detailedOutput;
	}

	/**
	 * @param detailedOutput the detailedOutput to set
	 */
	public void setDetailedOutput(boolean detailedOutput) {
		this.detailedOutput = detailedOutput;
	}

	/**
	 * Perform LoopyBeliefPropagation:
	 */
	public void performLoopyBeliefPropagation() {
		boolean hasConverged = false;
		while ( !hasConverged ) {
			if ( detailedOutput ) {
				printMessageDetails();
			}
			performOneIteration();
			computePotentials();
			hasConverged = checkForConvergence();
		}
		if ( detailedOutput ) {
			printMessageDetails();
		}
	}
	
	private void printMessageDetails() {
		int msg_index = 0;
		for ( Message msg : this.messages ) {
			System.out.println("-------------------------------------------------------------------------------------");
			System.out.println(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - Printing Message " + msg_index + " details: ");
			System.out.println( "isFactorToVariable = " + msg.isFactorToVariable() );
			System.out.println( "factorIndex = " + msg.getFactorIndex() );
			System.out.println( "variableIndex = " + msg.getVariableIndex() );
			System.out.println( "isEmptyContent = " + msg.isEmptyContent() );
			//System.out.println( "isVariableToFactor = " + msg.isVariableToFactor() );
			//System.out.println( "contentDimensionality = " + msg.getContent().getDimensionality() );
			//System.out.println( "contentIsOne = " + msg.getContent().isOne() );
			//System.out.println( "contentClass = " + msg.getContent().getClass() );
			System.out.println( "contentNormalisationConstant = " + ( (GaussianDensity) msg.getContent() ).getNormalisationConstant() );
			//System.out.println( "contentMean = " + ( (GaussianDensity) msg.getContent() ).getMean() );
			System.out.println( "contentMeanAt0 = " + ( (GaussianDensity) msg.getContent() ).getMean()[0] );
			//System.out.println( "contentVariance = " + ( (GaussianDensity) msg.getContent() ).getVariance() );
			System.out.println( "contentVarianceAt0 = " + ( (GaussianDensity) msg.getContent() ).getVariance()[0] );
			msg_index++;
		}
	}

	/**
	 * Perform a single iteration of LoopyBeliefPropagation:
	 */
	public void performOneIteration() {
		this.currentIteration++;
		if ( detailedOutput ) {
			System.out.println("                                 --- -- - -- ---                                     ");
			System.out.println("-------------------------------------------------------------------------------------");
			System.out.println( "                              Current iteration = " + this.currentIteration );
			System.out.println("-------------------------------------------------------------------------------------");
			System.out.println("                                 --- -- - -- ---                                     ");
		}
		ArrayList<Message> newMessages = new ArrayList<Message>();
		for ( int msg = 0; msg < n_messages; msg++) {
			// What msg type ~if~ -> call appropriate messageComputer:
			Message message = messages.get(msg);
			int variableIndex = message.getVariableIndex();
			int factorIndex = message.getFactorIndex();
			
			if ( message.isFactorToVariable() ) {
				message = computeAggregateIncomingMessageToVariableFromFactor( variableIndex, factorGraph.getFactor(factorIndex) );
			} else {
				message = computeAggregateIncomingMessageToFactorFromVariable( factorGraph.getFactor(factorIndex), variableIndex );
			}
			newMessages.add(message);
		}
		messages = newMessages;
	}
	
	/**
	 * Check for convergence:
	 */
	public boolean checkForConvergence() {
		boolean hasConverged = false;
		// TODO implement a proper convergence check, using the change in the posterior mean. As is, this 
		// works with no issue, but only because we call JSkills for the pairwise comparison scenario.
		if ( this.currentIteration > 2 ) { // > 2 gives the exact posterior for the Rating scenario.
			hasConverged = true;
		}
		return hasConverged;
	}
	
	/**
	 * Compute the posterior for each variable:
	 */
	private void computePotentials() {
		ArrayList<ProbabilityDensityFunction> posterior = new ArrayList<ProbabilityDensityFunction>();
		for ( int var = 0; var < this.factorGraph.getN_variables(); var++) {
			posterior.add( Message.computeProductOfMessageContent( getIncomingMessagesToVariable(var) ) );
		}
		this.priorHistory.add(posterior);
		potentials = posterior;
	}
	
	/**
	 * Populate an ArrayList with all incoming Messages to the variable:
	 */
	private ArrayList<Message> getIncomingMessagesToVariable( int variableIndex ) {
		ArrayList<Message> messagesIn = new ArrayList<Message>();
		 for ( int fac = 0; fac < this.factorGraph.getN_factors(); fac++ ) {
			 if ( this.factorGraph.checkEdge( fac, variableIndex ) ) {
				 messagesIn.add( this.messages.get( Message.getIndexOfMatchingMessage( this.messages, fac, variableIndex, true ) ) );
			 }
		 }
		return messagesIn;
	}

	/**
	 * Compute the aggregate of incoming messages to a factor:
	 */
	public Message computeAggregateIncomingMessageToFactorFromVariable( Factor factorOut, int variableIndex ) {
		ArrayList<Message> messagesInToVariable = getIncomingMessagesToVariable( factorOut, variableIndex );
		Message aggregateMessage = computeMessageFromVariableToFactor( factorOut, variableIndex, messagesInToVariable );
		return aggregateMessage;
	}
	private Message computeMessageFromVariableToFactor( Factor factorOut, int variableIndex, ArrayList<Message> messagesInToVariable ) {
		ProbabilityDensityFunction message_pdf = messagesInToVariable.get(0).getContent();
		
		for ( int msg = 1; msg < messagesInToVariable.size(); msg++ ) {
			message_pdf = message_pdf.computeProductWith( messagesInToVariable.get(msg).getContent() );
		}
		
		return new Message( message_pdf, factorOut.getFactorIndex(), variableIndex, false, message_pdf.isOne() );
	}

	/**
	 * Populate an ArrayList with all incoming Messages to the variable (sans the one from the messaged factorOut):
	 */
	private ArrayList<Message> getIncomingMessagesToVariable(Factor factorOut, int variableIndex) {
		ArrayList<Message> messagesIn = new ArrayList<Message>();
		 for ( int fac = 0; fac < this.factorGraph.getN_factors(); fac++ ) {
			 if ( this.factorGraph.checkEdge( fac, variableIndex ) && fac != factorOut.getFactorIndex() ) {
				 messagesIn.add( this.messages.get( Message.getIndexOfMatchingMessage( this.messages, fac, variableIndex, true ) ) );
			 }
		 }
		return messagesIn;
	}

	/**
	 * Compute the aggregate of incoming messages to a variable:
	 */
	public Message computeAggregateIncomingMessageToVariableFromFactor( int variableOutIndex, Factor factor ) {
		ArrayList<Message> messagesInToFactor = getIncomingMessagesToFactor( variableOutIndex, factor );
		Message aggregateMessage;
		
		if ( messagesInToFactor.isEmpty() ) {
			aggregateMessage = factor.computeMessageOut( variableOutIndex );
		} else {
			aggregateMessage = factor.computeMessageOut( variableOutIndex, messagesInToFactor );
		}
		
		return aggregateMessage;
	}

	/**
	 * Populate an ArrayList with all incoming Messages to the Factor (sans the one from the messaged variableOut):
	 */
	private ArrayList<Message> getIncomingMessagesToFactor(int variableOutIndex, Factor factor) {
		ArrayList<Message> messagesIn = new ArrayList<Message>();
		 for ( int var = 0; var < this.factorGraph.getN_variables(); var++ ) {
			 if ( this.factorGraph.checkEdge( factor.getFactorIndex(), var ) && var != variableOutIndex ) {
				 messagesIn.add( this.messages.get( Message.getIndexOfMatchingMessage( this.messages, factor.getFactorIndex(), var, false ) ) );
			 }
		 }
		return messagesIn;
	}
	
}

