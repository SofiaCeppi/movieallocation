/**
 * 
 */
package inferenceFromUserActions.loopyBeliefPropagation;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.factorGraphGenerators.SumOf1DConditionalGaussiansAndPairwiseComparisonFactorGraphGenerator;
import inferenceFromUserActions.interactions.Rating;
import inferenceFromUserActions.interactions.Selection;
import inferenceFromUserActions.primitiveData.Assignment;
import inferenceFromUserActions.primitiveData.DiscreteAssignment;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;

/**
 * @author Cortu01
 *
 */
public class PrepareForLBP_fromSumOf1D_CG_pairwiseComparison extends PrepareObjectsForLoopyBeliefPropagation {

	/**
	 * Full constructor:
	 */
	public PrepareForLBP_fromSumOf1D_CG_pairwiseComparison( SumOfConditionalGaussians utilityFunction, Selection interaction ) {
		
		this.fullPrior = utilityFunction;
		this.interaction = interaction;
		
		SumOf1DConditionalGaussiansAndPairwiseComparisonFactorGraphGenerator factorGraphGenerator = 
				new SumOf1DConditionalGaussiansAndPairwiseComparisonFactorGraphGenerator( utilityFunction, interaction );
		this.factorGraph = factorGraphGenerator.getFactorGraph();
		
		this.variablePriors = new ArrayList<ProbabilityDensityFunction>();
		
		// It's a pairwise comparison (no draws) in the form of a selection from a list of 2 items, so pick the priors
		// for each item for each parameter and add those first to "variablePriors":
		Assignment item;
		int assignmentIndex;
		
		for ( int itm = 0; itm < 2; itm++ ) {
			item = interaction.getItemsList().get(itm);
			for ( int par = 0; par < factorGraph.getN_parameters(); par++) {
				// for each parameter, look at respective assignment, pick that
				// distribution from the utility function(parameter, assignment)
				assignmentIndex = ((Integer) item.getValue(par)).intValue();
				this.variablePriors.add(utilityFunction.getBelief(par, assignmentIndex));
			}
		}
		
		// Then, add the prior for the dependent variables (utility of items 0 and 1):
		GaussianDensity utilityPrior_0 = new GaussianDensity( 0.5, Rating.getNoise(), true );
		this.variablePriors.add( utilityPrior_0 );
		GaussianDensity utilityPrior_1 = new GaussianDensity( 0.5, Rating.getNoise(), true );
		this.variablePriors.add( utilityPrior_1 );
		
	}

	@Override
	public void savePosterior(Object updatedPart) {
		try {
			if ( ! ( updatedPart instanceof ArrayList<?> ) || ! ( ( (ArrayList) updatedPart).get(0) instanceof ProbabilityDensityFunction ) ) {
				String errorText = ">>> Error while updating our belief over the user's utility function. "
						+ "The updated part of our belief was not given as an ArrayList<ProbabilityDensityFunction>";
				throw new IllegalArgumentException(errorText);
			} else {			
				Assignment item_0 = ( (Selection) this.interaction ).getItemsList().get(0);
				Assignment item_1 = ( (Selection) this.interaction ).getItemsList().get(1);
				try {
					if( ! ( ( item_0 instanceof DiscreteAssignment ) || ( item_1 instanceof DiscreteAssignment ) ) ) {
						String errorText = ">>> Error while updating our belief over the user's utility function. "
								+ "one of the compared items was not defined in a discrete assignment space.";
						throw new IllegalArgumentException(errorText);
					} else {
						this.fullPosterior = this.fullPrior;
						for ( int par = 0; par < item_0.getValues().size(); par++ ) { // for each assignment to a parameter
							//Take that part of the prior and replace it with the respective part of the updated potential.
							( (SumOfConditionalGaussians) this.fullPosterior ).setBelief( ( (List<ProbabilityDensityFunction>) updatedPart ).get(par), par, ( (int) item_0.getValue(par) ) );
						}
						for ( int par = 0; par < item_1.getValues().size(); par++ ) { // for each assignment to a parameter
							//Take that part of the prior and replace it with the respective part of the updated potential.
							( (SumOfConditionalGaussians) this.fullPosterior ).setBelief( ( (List<ProbabilityDensityFunction>) updatedPart ).get( par + item_0.getValues().size() ), par, ( (int) item_1.getValue(par) ) );
						}
					} 
				} catch ( IllegalArgumentException e ) {
					throw new RuntimeException(e);
				}
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		
	}

}
