/**
 * 
 */
package inferenceFromUserActions.loopyBeliefPropagation;

import java.util.List;

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.factorGraphGenerators.FactorGraph;
import inferenceFromUserActions.interactions.Interaction;

/**
 * @author Cortu01
 *
 */
public abstract class PrepareObjectsForLoopyBeliefPropagation {
	protected FactorGraph factorGraph;
	protected List<ProbabilityDensityFunction> variablePriors; // Prior only for the variables appearing in the factorGraph.
	protected Object fullPrior;
	protected Object fullPosterior;
	protected Interaction interaction;
	
	/**
	 * @return the interaction
	 */
	public Interaction getInteraction() {
		return interaction;
	}

	/**
	 * @param interaction the interaction to set
	 */
	public void setInteraction(Interaction interaction) {
		this.interaction = interaction;
	}

	/**
	 * @return the fullPrior
	 */
	public Object getFullPrior() {
		return fullPrior;
	}
	
	/**
	 * @param fullPrior the fullPrior to set
	 */
	public void setFullPrior(Object fullPrior) {
		this.fullPrior = fullPrior;
	}
	
	/**
	 * @return the fullPosterior
	 */
	public Object getFullPosterior() {
		return fullPosterior;
	}
	
	/**
	 * @param fullPosterior the fullPosterior to set
	 */
	public void setFullPosterior(Object fullPosterior) {
		this.fullPosterior = fullPosterior;
	}
	
	/**
	 * @return the factorGraph
	 */
	public FactorGraph getFactorGraph() {
		return factorGraph;
	}
	
	/**
	 * @return the variablePriors
	 */
	public List<ProbabilityDensityFunction> getVariablePriors() {
		return variablePriors;
	}
	
	public abstract void savePosterior( Object updatedPart );

}
