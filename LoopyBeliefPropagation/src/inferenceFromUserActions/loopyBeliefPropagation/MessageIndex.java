/**
 * This isn't currently used.
 * 
 */
package inferenceFromUserActions.loopyBeliefPropagation;

/**
 * @author Cortu01
 *
 */
public class MessageIndex {
	private int factorIndex;
	private int variableIndex;
	private boolean isFactorToVariable;
	private boolean isVariableToFactor;
	
	/**
	 * Empty constructor:
	 */
	public MessageIndex() {}

	/**
	 * Full constructor:
	 */
	public MessageIndex( int factorIndex, int variableIndex, boolean isFactorToVariable ) {
		this.factorIndex = factorIndex;
		this.variableIndex = variableIndex;
		this.isFactorToVariable = isFactorToVariable;
		this.isVariableToFactor = !isFactorToVariable;
	}
	
	/**
	 * @return the factorIndex
	 */
	public int getFactorIndex() {
		return factorIndex;
	}
	/**
	 * @param factorIndex the factorIndex to set
	 */
	public void setFactorIndex(int factorIndex) {
		this.factorIndex = factorIndex;
	}
	/**
	 * @return the variableIndex
	 */
	public int getVariableIndex() {
		return variableIndex;
	}
	/**
	 * @param variableIndex the variableIndex to set
	 */
	public void setVariableIndex(int variableIndex) {
		this.variableIndex = variableIndex;
	}
	/**
	 * @return the isFactorTovariable
	 */
	public boolean isFactorTovariable() {
		return isFactorToVariable;
	}
	/**
	 * @param isFactorTovariable the isFactorTovariable to set
	 */
	public void setFactorToVariable(boolean isFactorToVariable) {
		this.isFactorToVariable = isFactorToVariable;
		this.isVariableToFactor = !isFactorToVariable;
	}
	/**
	 * @return the isVariabletoFactor
	 */
	public boolean isVariableToFactor() {
		return isVariableToFactor;
	}
	/**
	 * @param isVariabletoFactor the isVariabletoFactor to set
	 */
	public void setVariableToFactor(boolean isVariableToFactor) {
		this.isVariableToFactor = isVariableToFactor;
	}

}
