/**
 * 
 */
package inferenceFromUserActions.loopyBeliefPropagation;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.factorGraphGenerators.SumOf1DConditionalGaussiansAndRatingGraphGenerator;
import inferenceFromUserActions.interactions.Rating;
import inferenceFromUserActions.primitiveData.Assignment;
import inferenceFromUserActions.primitiveData.DiscreteAssignment;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;

/**
 * @author Cortu01
 *
 */
public class PrepareForLBP_fromSumOf1D_CG_rating extends PrepareObjectsForLoopyBeliefPropagation {

	/**
	 * Full constructor:
	 */
	public PrepareForLBP_fromSumOf1D_CG_rating( SumOfConditionalGaussians utilityFunction, Rating interaction ) {
		
		this.fullPrior = utilityFunction;
		this.interaction = interaction;
		
		SumOf1DConditionalGaussiansAndRatingGraphGenerator factorGraphGenerator  = new SumOf1DConditionalGaussiansAndRatingGraphGenerator( utilityFunction, interaction );
		this.factorGraph = factorGraphGenerator.getFactorGraph();
		
		this.variablePriors = new ArrayList<ProbabilityDensityFunction>();
		
		// It's a rating, so pick the priors for each parameter and add those first to "variablePriors":
		Assignment ratedItem = interaction.getItemsList().get(0);
		int assignmentIndex;
		
		for ( int par = 0; par < factorGraph.getN_parameters(); par++) {
			// for each parameter, look at respective assignment, pick that distribution from the utility function(parameter, assignment)
			assignmentIndex = ( (Integer) ratedItem.getValue(par) ).intValue(); // should be (int)
			this.variablePriors.add( utilityFunction.getBelief( par, assignmentIndex ) );
		}
		
		// Then, add the prior for the dependent variable (utility):
		GaussianDensity utilityPrior = new GaussianDensity( 0.5, Rating.getNoise(), true );
		this.variablePriors.add( utilityPrior );		
		
	}

	@Override
	public void savePosterior(Object updatedPart) {
		try {
			if ( ! ( updatedPart instanceof ArrayList<?> ) || ! ( ( (ArrayList) updatedPart).get(0) instanceof ProbabilityDensityFunction ) ) {
				String errorText = ">>> Error while updating our belief over the user's utility function. "
						+ "The updated part of our belief was not given as an ArrayList<ProbabilityDensityFunction>";
				throw new IllegalArgumentException(errorText);
			} else {			
				Assignment ratedItem = ( (Rating) this.interaction ).getRatedItem();
				try {
					if( ! ( ratedItem instanceof DiscreteAssignment ) ) {
						String errorText = ">>> Error while updating our belief over the user's utility function. "
								+ "The item rated was not defined in a discrete assignment space.";
						throw new IllegalArgumentException(errorText);
					} else {
						this.fullPosterior = this.fullPrior;
						for ( int par = 0; par < ratedItem.getValues().size(); par++ ) { // for each assignment to a parameter
							//Take that part of the prior and replace it with the respective part of the updated potential.
							( (SumOfConditionalGaussians) this.fullPosterior ).setBelief( ( (List<ProbabilityDensityFunction>) updatedPart ).get(par), par, ( (int) ratedItem.getValue(par) ) );
						}
					} 
				} catch ( IllegalArgumentException e ) {
					throw new RuntimeException(e);
				}
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		
	}

}
