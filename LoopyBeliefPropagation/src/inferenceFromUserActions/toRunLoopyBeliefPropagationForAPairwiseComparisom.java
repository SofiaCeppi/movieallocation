package inferenceFromUserActions;

import java.util.ArrayList;
import java.util.List;

import inferenceFromUserActions.beliefFunctions.ProbabilityDensityFunction;
import inferenceFromUserActions.interactions.Selection;
import inferenceFromUserActions.interactions.TaxedSelection;
import inferenceFromUserActions.loopyBeliefPropagation.PrepareForLBP_fromSumOf1D_CG_pairwiseComparison;
import inferenceFromUserActions.primitiveData.Assignment;
import inferenceFromUserActions.primitiveData.DiscreteAssignment;
import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Parameter;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;

/**
 * Example code for running a loopy belief propagation algorithm in order to perform a step of utility 
 * function belief update, following a user's action of selecting an item from a list.
 * 
 */

/**
 * @author Cortu01
 *
 */

public class toRunLoopyBeliefPropagationForAPairwiseComparisom {

	public toRunLoopyBeliefPropagationForAPairwiseComparisom() {
		
		String[] string = { "Test", "Test" };
		
		main(string);
	}

	public static void main( String[] args ) {
		// An example use of LoopyBeliefPropagation for updating our belief
		// over a user's utility function after they have rated an item.
		int n_parameters = 4;
		//int[] cardinalityPerParameter = { 5, 4, 3, 4 };
		int[] cardinalityPerParameter = { 3, 3, 3, 3 };
		
		Parameter utilityParameter = new Parameter( "Utility", "double", false, "Real" );
		
		// The definitions below imply much more information then what we are actually working with.
		// It is up to possible future extensions to make use of such descriptions. For now, we are 
		// simply working with discrete assignment domains from 0 to some other integer.
		List<Parameter> parameters = new ArrayList<Parameter>();
		String assignmentSpace3 = "PositiveInteger"; 
		int[] assignmentSpace1 = {0, 4};
		Parameter parameter1 = new Parameter( "Parameter1", "int", false, assignmentSpace3 );//true, assignmentSpace1
		String[] assignmentSpace2 = {"Blue", "Red", "Green", "Grey"}; 
		Parameter parameter2 = new Parameter( "Parameter2", "String", true, assignmentSpace1);//false, assignmentSpace2 );
		
		Parameter parameter3 = new Parameter( "Price", "int", true, assignmentSpace1);//false, assignmentSpace3 );
		String assignmentSpace4 = "Integer"; 
		Parameter parameter4 = new Parameter( "Size", "int", true, assignmentSpace1);//false, assignmentSpace4 );
		parameters.add(parameter1);
		parameters.add(parameter2);
		parameters.add(parameter3);
		parameters.add(parameter4);
		
		Domain domain = new Domain( parameters );
		
		int userIndex = 2;
		String fileWithMEANs = new String("C:/Users/Cortu01/Documents/movieallocation/movieallocationData/000/genome/alpha.csv");
		//SumOfConditionalGaussians utilityFunction = new SumOfConditionalGaussians( n_parameters, cardinalityPerParameter, utilityParameter, domain );
		SumOfConditionalGaussians utilityFunction = new SumOfConditionalGaussians( n_parameters, cardinalityPerParameter, utilityParameter, domain, fileWithMEANs, userIndex ); // fileWithVARs,
		
		ArrayList<Object> values_0 = new ArrayList<Object>();
		values_0.add(1);
		values_0.add(0);
		values_0.add(2);
		//values_0.add(3);
		values_0.add(2);
		
		ArrayList<Object> values_1 = new ArrayList<Object>();
		values_1.add(0);
		values_1.add(1);
		values_1.add(1);
		values_1.add(2);
		
		List<Assignment> itemsList = new ArrayList<Assignment>();
		itemsList.add( new DiscreteAssignment( domain, values_0 ) );
		itemsList.add( new DiscreteAssignment( domain, values_1 ) );
		
		List<Double> taxation = new ArrayList<Double>();
		taxation.add(  0.00 );
		taxation.add( -2.00 );
		
		int itemSelected = 1;
		
		SumOfConditionalGaussians updatedUtilityFunction = utilityFunction;
		//for ( int iteration = 0; iteration < 1; iteration++ ) {
			
			
			Selection interaction = new TaxedSelection(itemsList, taxation, itemSelected);
			//Selection interaction = new Selection(itemsList, itemSelected);

			PrepareForLBP_fromSumOf1D_CG_pairwiseComparison problemSpecification = new PrepareForLBP_fromSumOf1D_CG_pairwiseComparison(
					updatedUtilityFunction, interaction);
		
			JSkillsIntegrator JSkills = new JSkillsIntegrator( problemSpecification );
			JSkills.run();
			
			List<ProbabilityDensityFunction> potentials = JSkills.getPotentials();			
			problemSpecification.savePosterior(potentials);

			updatedUtilityFunction = (SumOfConditionalGaussians) problemSpecification.getFullPosterior();
			updatedUtilityFunction.printToConsole();
			
			itemSelected = 0;
		//}

	}

}
