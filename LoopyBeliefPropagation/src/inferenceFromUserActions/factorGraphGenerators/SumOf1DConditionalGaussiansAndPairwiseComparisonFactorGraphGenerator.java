/**
 * 
 */
package inferenceFromUserActions.factorGraphGenerators;

import java.util.ArrayList;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.interactions.Rating;
import inferenceFromUserActions.interactions.Selection;
import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Factor;
import inferenceFromUserActions.primitiveData.Factor_pdf;
import inferenceFromUserActions.primitiveData.Factor_sum;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;

/**
 * @author Cortu01
 *
 */
public class SumOf1DConditionalGaussiansAndPairwiseComparisonFactorGraphGenerator {
	private SumOfConditionalGaussians utilityFunction;
	private Selection interaction;
	private FactorGraph factorGraph;

	/**
	 * Full constructor:
	 */
	public SumOf1DConditionalGaussiansAndPairwiseComparisonFactorGraphGenerator( 
			SumOfConditionalGaussians utilityFunction, Selection interaction ) {
		this.utilityFunction = utilityFunction;
		try {
			if ( interaction.getItemListSize() != 2 ) {
				String errorText = ">>> Error while creating new SumOf1DConditionalGaussiansAndPairwiseComparisonFactorGraphGenerator. "
						+ "The Selection Interaction should involve exactly 2 items.";
				throw new IllegalArgumentException(errorText);
			} else {
				this.interaction = interaction;
			}
		} catch (IllegalArgumentException e){
			throw new RuntimeException(e);
		}
		
		this.factorGraph = generateFactorGraph( utilityFunction, interaction );
	}
	
	private FactorGraph generateFactorGraph( SumOfConditionalGaussians utilityFunction, Selection interaction ) {
		
		factorGraph = new FactorGraph();
		
		factorGraph.setFullDomain( utilityFunction.getIndependentVariables() );
		
		//Domain updatedSubDomain = factorGraph.getFullDomain().getSubDomainFromTrimmingParametersWithMatchingAssignments( 
		//		interaction.getItemsList().get(0), interaction.getItemsList().get(1) );
		Domain updatedSubDomain = factorGraph.getFullDomain();
		
		factorGraph.setUpdatedSubDomain(updatedSubDomain);
		
		int n_parameters = updatedSubDomain.getN_parameters();
		int n_nonParameterVariables = 3;
		int n_variables = n_nonParameterVariables + 2*n_parameters;
		
		factorGraph.setN_nonParameterVariables(n_nonParameterVariables);
		factorGraph.setN_variables(n_variables);
		
		int n_factors = n_variables + 1;
		factorGraph.setN_factors(n_factors);
		
		boolean[][] factorGraphEdges = new boolean[n_factors][n_variables];
		int var;
		for (int fac = 0; fac < n_factors; fac++) { // Initialise all as false:
			for (var = 0; var < n_variables; var++)
			factorGraphEdges[fac][var] = false;
		}
		for (int fac = 0; fac < 2*n_parameters; fac++) { // Parameters connect to their priors:
			var = fac;
			factorGraphEdges[fac][var] = true;
		}
		for (var = 0; var < n_parameters; var++) { // All variables representing the first assignment connect to the first sum of 
			// partial utilities factor:
			factorGraphEdges[2*n_parameters + 1 - 1][var] = true;
		}
		for (var = n_parameters; var < 2*n_parameters; var++) { // All variables representing the second assignment connect to the 
			// second sum of partial utilities factor:
			factorGraphEdges[2*n_parameters + 2 - 1][var] = true;
		}		
		// First sum factor connects to the first utility variable:
		factorGraphEdges[2*n_parameters + 1 - 1][2*n_parameters + 1 - 1] = true;
		// Second sum factor connects to the second utility variable:
		factorGraphEdges[2*n_parameters + 2 - 1][2*n_parameters + 2 - 1] = true;
		
		// Comparison factor connects to the first utility variable:
		factorGraphEdges[2*n_parameters + 3 - 1][2*n_parameters + 1 - 1] = true;
		// Comparison factor connects to the second utility variable:
		factorGraphEdges[2*n_parameters + 3 - 1][2*n_parameters + 2 - 1] = true;
		// Comparison factor connects to the response variable:
		factorGraphEdges[2*n_parameters + 3 - 1][n_variables - 1] = true;
		// Response factor connects to the response variable:
		factorGraphEdges[2*n_parameters + 4 - 1][n_variables - 1] = true;
				
		factorGraph.setFactorGraphEdges(factorGraphEdges);
		
		ArrayList<Factor> factorList = new ArrayList<Factor>();
		for ( int fac = 0; fac < n_parameters; fac++ ) {
			Factor factor = new Factor_pdf( fac, utilityFunction.getBelief( fac, (int)interaction.getItemsList().get(0).getValue(fac) ) );
			factorList.add(factor);			
		}
		for ( int fac = n_parameters; fac < 2*n_parameters; fac++ ) {
			Factor factor = new Factor_pdf( fac, utilityFunction.getBelief( fac - n_parameters, (int)interaction.getItemsList().get(1).getValue(fac - n_parameters) ) );
			factorList.add(factor);			
		}
		factorList.add( new Factor_sum( n_factors - 4, n_variables - 3 ) );	
		factorList.add( new Factor_sum( n_factors - 3, n_variables - 2 ) );
		factorList.add( new Factor_binaryComparison( n_factors - 2 ) );
		factorList.add( new Factor_pdf( n_factors - 1, new GaussianDensity( interaction.getAction(), Rating.getNoise(), false ) ) );
		
		factorGraph.setFactors(factorList);
		
		return factorGraph;
	}

	/**
	 * @return the factorGraph
	 */
	public FactorGraph getFactorGraph() {
		return factorGraph;
	}

	/**
	 * @return the utilityFunction
	 */
	public SumOfConditionalGaussians getUtilityFunction() {
		return utilityFunction;
	}

	/**
	 * @return the interaction
	 */
	public Selection getInteraction() {
		return interaction;
	}
	
}
