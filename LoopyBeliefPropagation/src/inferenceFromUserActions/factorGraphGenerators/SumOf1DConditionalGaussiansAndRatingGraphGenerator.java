/**
 * Also generates response potential...
 */
package inferenceFromUserActions.factorGraphGenerators;

import java.util.ArrayList;

import inferenceFromUserActions.beliefFunctions.GaussianDensity;
import inferenceFromUserActions.interactions.Rating;
import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Factor;
import inferenceFromUserActions.primitiveData.Factor_pdf;
import inferenceFromUserActions.primitiveData.Factor_sum;
import inferenceFromUserActions.utilityFunctions.SumOfConditionalGaussians;

/**
 * @author Cortu01
 *
 */
public class SumOf1DConditionalGaussiansAndRatingGraphGenerator {
	private SumOfConditionalGaussians utilityFunction;
	private Rating interaction;
	private FactorGraph factorGraph;
	
	/**
	 * Full constructor:
	 */
	public SumOf1DConditionalGaussiansAndRatingGraphGenerator( SumOfConditionalGaussians utilityFunction, Rating interaction ) {
		this.utilityFunction = utilityFunction;
		this.interaction = interaction;
		this.factorGraph = generateFactorGraph( utilityFunction, interaction );
	}

	private FactorGraph generateFactorGraph( SumOfConditionalGaussians utilityFunction, Rating interaction ) {
		
		factorGraph = new FactorGraph();
		
		factorGraph.setFullDomain( utilityFunction.getIndependentVariables() );
		
		Domain updatedSubDomain = factorGraph.getFullDomain();
		
		factorGraph.setUpdatedSubDomain(updatedSubDomain);
		
		int n_parameters = updatedSubDomain.getN_parameters();
		int n_nonParameterVariables = 1;
		int n_variables = n_nonParameterVariables + n_parameters;
		
		factorGraph.setN_parameters( n_parameters );
		factorGraph.setN_nonParameterVariables(n_nonParameterVariables);
		factorGraph.setN_variables(n_variables);
		
		int n_factors = n_parameters + 2;
		factorGraph.setN_factors(n_factors);
		
		boolean[][] factorGraphEdges = new boolean[n_factors][n_variables];
		
		for (int fac = 0; fac < n_factors; fac++) { // Initialise all as false:
			for (int var = 0; var < n_variables; var++)
			factorGraphEdges[fac][var] = false;
		}
		for (int fac = 0; fac < n_parameters; fac++) { // Parameters connect to their priors:
			int var = fac;
			factorGraphEdges[fac][var] = true;
		}
		
		for (int var = 0; var < n_variables; var++) { // All variables connect to the sum of partial utilities factor:
			factorGraphEdges[ n_parameters + 1 - 1 ][var] = true;
		} // The response factor connects to the utility variable:
		factorGraphEdges[n_factors - 1][n_variables - 1] = true;
		factorGraph.setFactorGraphEdges(factorGraphEdges);
		
		ArrayList<Factor> factorList = new ArrayList<Factor>();
		for ( int fac = 0; fac < n_parameters; fac++ ) {
			Factor factor = new Factor_pdf( fac, utilityFunction.getBelief( fac, 
					(int)interaction.getItemsList().get(0).getValue(fac) ).copy() );
			factorList.add(factor);			
		}
		
		factorList.add( new Factor_sum( n_factors - 2, n_variables - 1 ) );		
		factorList.add( new Factor_pdf( n_factors - 1, new GaussianDensity( interaction.getAction(), Rating.getNoise(), false ) ) );
		factorGraph.setFactors(factorList);
				
		return factorGraph;
	}

	/**
	 * @return the factorGraph
	 */
	public FactorGraph getFactorGraph() {
		return factorGraph;
	}

	/**
	 * @return the utilityFunction
	 */
	public SumOfConditionalGaussians getUtilityFunction() {
		return utilityFunction;
	}

	/**
	 * @return the interaction
	 */
	public Rating getInteraction() {
		return interaction;
	}

}
