/**
 * 
 */
package inferenceFromUserActions.factorGraphGenerators;

import java.util.ArrayList;

import inferenceFromUserActions.primitiveData.Domain;
import inferenceFromUserActions.primitiveData.Factor;

/**
 * @author Cortu01
 *
 */
public class FactorGraph {
	private Domain fullDomain;
	private Domain updatedSubDomain;
	
	private int n_variables, n_parameters, n_nonParameterVariables, n_factors;
	
	// Parameters here refers to all root variables whose posterior we want to compute.
	
	private boolean[][] factorGraphEdges;
	private ArrayList<Factor> factors;

	/**
	 * Empty constructor:
	 */
	public FactorGraph() {}
	
//	/**
//	 * Full constructor:
//	 */
//	public FactorGraph() {
//		// TODO Auto-generated constructor stub
//	}

	/**
	 * @return the fullDomain
	 */
	public Domain getFullDomain() {
		return fullDomain;
	}

	/**
	 * @param fullDomain the fullDomain to set
	 */
	public void setFullDomain(Domain fullDomain) {
		this.fullDomain = fullDomain;
		// this.n_parameters = fullDomain.getN_parameters(); <- Not for PairwiseComparisons
	}

	/**
	 * @return the updatedSubDomain
	 */
	public Domain getUpdatedSubDomain() {
		return updatedSubDomain;
	}

	/**
	 * @param updatedSubDomain the updatedSubDomain to set
	 */
	public void setUpdatedSubDomain(Domain updatedSubDomain) {
		this.updatedSubDomain = updatedSubDomain;
	}

	/**
	 * @return the n_variables
	 */
	public int getN_variables() {
		return n_variables;
	}

	/**
	 * @param n_variables the n_variables to set
	 */
	public void setN_variables(int n_variables) {
		this.n_variables = n_variables;
	}

	/**
	 * @return the n_nonParameterVariables
	 */
	public int getN_nonParameterVariables() {
		return n_nonParameterVariables;
	}

	/**
	 * @param n_nonParameterVariables the n_nonParameterVariables to set
	 */
	public void setN_nonParameterVariables(int n_nonParameterVariables) {
		this.n_nonParameterVariables = n_nonParameterVariables;
	}

	/**
	 * @return the n_factors
	 */
	public int getN_factors() {
		return n_factors;
	}

	/**
	 * @param n_factors the n_factors to set
	 */
	public void setN_factors(int n_factors) {
		this.n_factors = n_factors;
	}

	/**
	 * @return the n_parameters
	 */
	public int getN_parameters() {
		return n_parameters;
	}

	/**
	 * @param n_parameters the n_parameters to set
	 */
	public void setN_parameters(int n_parameters) {
		this.n_parameters = n_parameters;
	}
	
	/**
	 * @return the factorGraphEdges
	 */
	public boolean[][] getFactorGraphEdges() {
		return factorGraphEdges;
	}

	/**
	 * @param factorGraphEdges the factorGraphEdges to set
	 */
	public void setFactorGraphEdges(boolean[][] factorGraphEdges) {
		this.factorGraphEdges = factorGraphEdges;
	}

	/**
	 * @return the factors
	 */
	public ArrayList<Factor> getFactors() {
		return factors;
	}

	/**
	 * @param factors the factors to set
	 */
	public void setFactors(ArrayList<Factor> factors) {
		this.factors = factors;
	}
	
	/**
	 * @return the factors
	 */
	public Factor getFactor(int index) {
		return factors.get(index);
	}

	/**
	 * @param factors the factors to set
	 */
	public void setFactors( int index, Factor factor ) {
		this.factors.set(index, factor);
	}

	public boolean checkEdge(int factorIndex, int variableIndex) {
		return this.getFactorGraphEdges()[factorIndex][variableIndex];
	}

}
