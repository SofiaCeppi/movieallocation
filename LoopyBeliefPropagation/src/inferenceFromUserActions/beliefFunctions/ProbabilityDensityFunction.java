package inferenceFromUserActions.beliefFunctions;
/**
 * Abstract class for storing a Probability Density Function (PDF). Defines abstract methods for computing its 
 * product with another PDF, and summing the corresponding random variable with another random variable, 
 * as represented by its density.
 * 
 */

/**
 * @author Cortu01
 *
 */
public abstract class ProbabilityDensityFunction {

	protected final int dimensionality;
	protected boolean isOne;
	
	/**
	 * 'protected final' constructor:
	 */
	protected ProbabilityDensityFunction( int dimensionality ) {
		this.dimensionality = dimensionality;
	}
	
	public abstract ProbabilityDensityFunction computeProductWith(ProbabilityDensityFunction otherDensity);
	
	public abstract ProbabilityDensityFunction sumWith(ProbabilityDensityFunction otherDensity);
	
	public int getDimensionality() {
		return dimensionality;
	}

//	public void setDimensionality(int dimensionality) {
//		this.dimensionality = dimensionality;
//	}
	
	public boolean isOne() {
		return isOne;
	}

	public void setOne(boolean isOne) {
		this.isOne = isOne;
	}

	abstract public ProbabilityDensityFunction copy();

	abstract public ProbabilityDensityFunction scale(double d);

}
