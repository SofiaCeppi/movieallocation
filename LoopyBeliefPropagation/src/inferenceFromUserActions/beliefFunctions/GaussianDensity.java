package inferenceFromUserActions.beliefFunctions;
import java.lang.Math;

import org.apache.commons.math3.distribution.NormalDistribution;

import utilities.ArrayMethods;

/**
 * Stores a Gaussian Probability Density Function. Provides methods for summing the corresponding Gaussian random variable to 
 * another Gaussian random variable, and for computing the product of this density to another Gaussian Probability Density 
 * Function.
 * 
 */

/**
 * @author Cortu01
 *
 */
public class GaussianDensity extends ProbabilityDensityFunction {

	private double mean[];
	private double variance[];
	private double normalisationConstant = 1.0;


	/**
	 * Lazy constructor:
	 */
	public GaussianDensity( double mean, double variance, boolean isOne ) {
		super(1);
		double meanArray[] = new double[1];
		meanArray[0] = mean;
		this.mean = meanArray;
		double varianceArray[] = new double[1];
		varianceArray[0] = variance;
		this.variance = varianceArray;
		this.isOne = isOne;
	}
	
	/**
	 * Full constructor:
	 */
	public GaussianDensity( double[] mean, double[] variance, int dimensionality, boolean isOne, double normalisationConstant ) {
		this( mean, variance );
		try {
			if ( this.dimensionality != dimensionality ) {
				String errorText = ">>> Error while creating new GaussianDensity. The dimensionality specified does not "
						+ "match the length of the 'mean' and 'variance' vectors.";
				throw new IllegalArgumentException(errorText);
				
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		this.isOne = isOne;
		this.setNormalisationConstant(normalisationConstant);
	}
	
	/**
	 * Full - 'normalisation constant' constructor:
	 */
	public GaussianDensity( double[] mean, double[] variance, int dimensionality, boolean isOne ) {
		this( mean, variance );
		try {
			if ( this.dimensionality != dimensionality ) {
				String errorText = ">>> Error while creating new GaussianDensity. The dimensionality specified does not "
						+ "match the length of the 'mean' and 'variance' vectors.";
				throw new IllegalArgumentException(errorText);
				
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		this.isOne = isOne;
	}
	
	/**
	 * Basic + 'normalisation constant' constructor:
	 */
	public GaussianDensity( double[] mean, double[] variance, double normalisationConstant ) {
		this( mean, variance );
		this.setNormalisationConstant(normalisationConstant);
		this.isOne = false;
	}
	
	/**
	 * Basic constructor:
	 */
	public GaussianDensity( double[] mean, double[] variance ) {
		super( mean.length );
		try {
			if ( mean.length != variance.length ) {
				String errorText = ">>> Error while creating new GaussianDensity. The dimensionalities specified by the "
						+ "length of the 'mean' and 'variance' vectors do not match.";
				throw new IllegalArgumentException(errorText);
			} else {
				this.mean = mean;
				this.variance = variance;
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		this.isOne = false;
	}

	/* (non-Javadoc)
	 * @see ProbabilityDensityFunction#sumWith(ProbabilityDensityFunction)
	 */
	@Override
	public ProbabilityDensityFunction sumWith(ProbabilityDensityFunction otherDensity) { // Sums the respective random variables.
		// Assumes independence of the 2 random variables defined by the densities.
		try {
			if ( otherDensity.isOne() ) {
				return otherDensity;
			} else if ( this.isOne() ) {
				return this;
			} else if ( otherDensity instanceof GaussianDensity ) {
				return computeSumOfGaussians( this, (GaussianDensity)otherDensity );
			} else {
				String errorText =  ">>> Error while computing sum of GaussianDensity with other Density: "
						+ "neither of the distributions has 'isOne' set to true and there is no method for handling "
						+ "the sum of GaussinaDensity with the type of distribution represented by the type "
						+ "extension for the other Density. Returning 'this' GaussinaDensity.";
				throw new IllegalArgumentException(errorText);
			}
		} catch ( IllegalArgumentException e) {
			return this;
		}
	}
	
	private ProbabilityDensityFunction computeSumOfGaussians( GaussianDensity gaussianDensity, GaussianDensity otherDensity ) {
		double[] sumMean;
		double[] sumVariance;
		
		try {
			if ( gaussianDensity.getDimensionality() != otherDensity.getDimensionality() ) {
				String errorText = ">>> Error while computing the product of the 2 provided GaussianDensity-ies. The "
						+ "densities are of different dimensionality.";
				throw new IllegalArgumentException(errorText);
			} else {			
				sumMean = ArrayMethods.elementWiseSum( 
						ArrayMethods.elementWiseProduct( gaussianDensity.normalisationConstant, gaussianDensity.mean ), 
						ArrayMethods.elementWiseProduct( otherDensity.normalisationConstant, otherDensity.mean ) 
						);
				sumVariance = ArrayMethods.elementWiseSum( 
						ArrayMethods.elementWiseProduct( Math.pow( gaussianDensity.normalisationConstant, 2 ), gaussianDensity.variance ), 
						ArrayMethods.elementWiseProduct( Math.pow( otherDensity.normalisationConstant, 2 ), otherDensity.variance) 
						);
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		
		ProbabilityDensityFunction gaussianProduct = new GaussianDensity( sumMean, sumVariance );
		return gaussianProduct;
	}
	
	/* (non-Javadoc)
	 * @see ProbabilityDensityFunction#computeProductWith(ProbabilityDensityFunction)
	 */
	@Override
	public ProbabilityDensityFunction computeProductWith( ProbabilityDensityFunction otherDensity ) {
		try {
			if ( otherDensity.isOne() ) {
				return this;
			} else if ( this.isOne() ) {
				return otherDensity;
			} else if ( otherDensity instanceof GaussianDensity ) {
				return computeProductOfGaussians( this, (GaussianDensity)otherDensity );
			} else {
				String errorText =  ">>> Error while computing product of GaussianDensity with other Density: "
						+ "neither of the distributions has 'isOne' set to true and there is no method for handling "
						+ "the product of GaussinaDensity with the type of distribution represented by the type "
						+ "extension for the other Density. Returning 'this' GaussinaDensity.";
				throw new IllegalArgumentException(errorText);
			}
		} catch ( IllegalArgumentException e) {
			return this;
		}
	}

	private ProbabilityDensityFunction computeProductOfGaussians( GaussianDensity gaussianDensity, GaussianDensity otherDensity ) {
		double[] productMean;
		double[] productVariance;
		double[] productPrecision;
		
		double[] gaussianPrecision;
		double[] otherPrecision;
		
		double normalisationConstant;
		double newNormalisationConstant;
		
		try {
			if ( gaussianDensity.getDimensionality() != otherDensity.getDimensionality() ) {
				String errorText = ">>> Error while computing the product of the 2 provided GaussianDensity-ies. The "
						+ "densities are of different dimensionality.";
				throw new IllegalArgumentException(errorText);
			} else {
				gaussianPrecision = ArrayMethods.inverse( gaussianDensity.variance );
				otherPrecision = ArrayMethods.inverse( otherDensity.variance );
				productPrecision = ArrayMethods.elementWiseSum( gaussianPrecision, otherPrecision );
				productVariance = ArrayMethods.inverse( productPrecision );
				
				productMean = ArrayMethods.elementWiseProduct( productVariance, 
						( ArrayMethods.elementWiseSum( ArrayMethods.elementWiseProduct( gaussianDensity.mean, gaussianPrecision ),
								ArrayMethods.elementWiseProduct( otherDensity.mean, otherPrecision ) ) 
								) 
						);
				
				NormalDistribution temp = new NormalDistribution( otherDensity.mean[0], Math.sqrt( 
						ArrayMethods.elementWiseSum( gaussianDensity.variance, otherDensity.variance  )[0] ) );
				newNormalisationConstant = temp.density( gaussianDensity.mean[0] );
				normalisationConstant = newNormalisationConstant 
						* gaussianDensity.normalisationConstant * otherDensity.normalisationConstant;
			}
		} catch ( IllegalArgumentException e ) {
			throw new RuntimeException(e);
		}
		
		ProbabilityDensityFunction gaussianProduct = new GaussianDensity( productMean, productVariance, normalisationConstant );
		return gaussianProduct;
	}

	
	public double[] getMean() {
		return mean;
	}

	public void setMean(double[] mean) {
		this.mean = mean;
	}
	
	public void setMean(double mean, int dim) {
		this.mean[dim] = mean;
	}

	public double[] getVariance() {
		return variance;
	}

	public void setVariance(double[] variance) {
		this.variance = variance;
	}
	
	public void setVariance(double variance, int dim) {
		this.variance[dim] = variance;
	}

	public double[] getStandardDeviation() {
		double[] standardDeviation = new double[this.dimensionality];
		
		int index = 0;
		for ( double var: this.variance ) {
			standardDeviation[index] = Math.sqrt( var );
			index++;
		}
		
		return standardDeviation;
	}

	public void setStandardDeviation(double[] standardDeviation) {
		double[] variance = new double[this.dimensionality];
		
		int index = 0;
		for ( double var: standardDeviation ) {
			variance[index] = Math.pow( var, 2.0 );
			index++;
		}
		this.variance = variance;
	}

	public double getNormalisationConstant() {
		return normalisationConstant;
	}

	public void setNormalisationConstant(double normalisationConstant) {
		this.normalisationConstant = normalisationConstant;
	}
	
	public GaussianDensity copy() {
		GaussianDensity pdf = new GaussianDensity( this.mean, this.variance, this.dimensionality, this.isOne, this.normalisationConstant );
		return pdf;
	}

	@Override
	public ProbabilityDensityFunction scale(double scale) {
		double[] newMean = new double[this.dimensionality]; // Am I guaranteed to have dimensionality of at least 1?
		double[] newVariance = new double[this.dimensionality];
		if ( this.isOne ) {
			newMean[0] = 0;
			newVariance[0] = 0;
		} else {
			newMean = ArrayMethods.elementWiseProduct(scale, this.mean);
			newVariance = ArrayMethods.elementWiseProduct(scale * scale, this.variance);
		}
		return new GaussianDensity( newMean, newVariance, 
				this.dimensionality, this.isOne, this.normalisationConstant );
	}
	
}
