/**
 * Defines methods for executing computations over java arrays. (Currently only of type double[].)
 */
package utilities;

/**
 * @author Cortu01
 *
 */
public final class ArrayMethods {

	public static int sumElements( boolean[][] array ) {
		int elementSum;

		try {
			elementSum = 0;
			for ( int i = 0; i < array.length; i++ ) {
				for ( int j =0; j < array[1].length; j++ ) {
					if ( array[i][j] ) {
						elementSum = elementSum + 1;
					}
				}
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementSum;
	}
	
	public static int sumElements( boolean[] array ) {
		int elementSum;

		try {
			elementSum = 0;
			for ( int i = 0; i < array.length; i++ ) {
				if ( array[i] ) {
					elementSum = elementSum + 1;
				}
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementSum;
	}
	
	public static int sumElements( int[][] array ) {
		int elementSum;

		try {
			elementSum = 0;
			for ( int i = 0; i < array.length; i++ ) {
				for ( int j =0; j < array[1].length; j++ ) {
					elementSum = elementSum + array[i][j];
				}
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementSum;
	}
	
	public static int sumElements( int[] array ) {
		int elementSum;

		try {
			elementSum = 0;
			for ( int i = 0; i < array.length; i++ ) {
				elementSum = elementSum + array[i];
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementSum;
	}
	
	public static double sumElements( double[][] array ) {
		double elementSum;

		try {
			elementSum = 0;
			for ( int i = 0; i < array.length; i++ ) {
				for ( int j =0; j < array[1].length; j++ ) {
					elementSum = elementSum + array[i][j];
				}
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementSum;
	}
	
	public static double sumElements( double[] array ) {
		double elementSum;

		try {
			elementSum = 0;
			for ( int i = 0; i < array.length; i++ ) {
				elementSum = elementSum + array[i];
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementSum;
	}

	public static double[] elementWiseProduct(double[] array1, double[] array2) {
		double[] elementWiseProduct;

		try {
			if ( array1.length != array2.length ) {
				String errorText = ">>> Error while computing element-wise array product. "
						+ "The array dimensionalities do not match.";
				throw new IllegalArgumentException(errorText);
			} else {
				elementWiseProduct = new double[array1.length];
				for ( int i = 0; i < array1.length; i++ ) {
					elementWiseProduct[i] = array1[i] * array2[i];
				}
			}
			
		} catch ( IllegalArgumentException e) {
			throw new RuntimeException(e);
		}
		
		return elementWiseProduct;
	}
	
	public static double[] elementWiseProduct( double constant, double[] array ) {
		double[] elementWiseProduct;

		try {
			elementWiseProduct = new double[array.length];
			for ( int i = 0; i < array.length; i++ ) {
				elementWiseProduct[i] = constant * array[i];
			}
			
		} catch ( Error e) {
			throw new RuntimeException(e);
		}
		
		return elementWiseProduct;
	}

	
	public static double[] elementWiseSum(double[] array1, double[] array2) {
		double[] elementWiseSum;

		try {
			if ( array1.length != array2.length ) {
				String errorText = ">>> Error while computing element-wise array sum. "
						+ "The array dimensionalities do not match.";
				throw new IllegalArgumentException(errorText);
			} else {
				elementWiseSum = new double[array1.length];
				for ( int i = 0; i < array1.length; i++ ) {
					elementWiseSum[i] = array1[i] + array2[i];
				}
			}
			
		} catch ( IllegalArgumentException e) {
			throw new RuntimeException(e);
		}
		
		return elementWiseSum;
	}
	
	public static double[] elementWiseSum(double constant, double[] array) {
		double[] elementWiseSum;

		try {
			elementWiseSum = new double[array.length];
			for ( int i = 0; i < array.length; i++ ) {
				elementWiseSum[i] = constant + array[i];
			}		
		} catch ( IllegalArgumentException e) {
			throw new RuntimeException(e);
		}
		
		return elementWiseSum;
	}

	public static double[] inverse(double[] array) {
		double[] arrayWithInverseValues = new double[array.length];
		for ( int index = 0; index < array.length; index++ ) {
			if ( array[index] == 0 ) {
				arrayWithInverseValues[index] = 1000; // TODO (?) Arbitrary "large" number.
			} else {
				arrayWithInverseValues[index] = 1/array[index];
			}
		}
		return arrayWithInverseValues;
	}

}